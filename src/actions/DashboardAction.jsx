import { actions, apiUrl } from './../constants/Constants';
import CommonUtils from './../utils/CommonUtils';
import Request from './../utils/Request';

export function requestHotelSummary() {
  return {
    type: actions.DASHBOARD_ITEMS_LOADING,
  };
}

export function receivedHotelSummary(hotelSummary) {
  return {
    type: actions.DASHBOARD_ITEMS_LOADED,
    payload: {
      receivedAt: Date.now(),
      hotelSummary,
    },
  };
}

export function failedHotelSummaryRequest() {
  return {
    type: actions.DASHBOARD_ITEMS_LOADING_FAILED,
  };
}

export function shouldFetchHotelSummary(state) {
  return (CommonUtils.isEmpty(state.dashboard.hotelSummary));
}

export function fetchHotelSummary(propertyId, startDate, endDate) {
  return (dispatch) => {
    const successFn = (responseData) => {
      const hotelSummary = !CommonUtils.isEmpty(responseData) ? responseData : {};

      dispatch(receivedHotelSummary(hotelSummary));
    };

    const errorFn = (error) => {
      dispatch(failedHotelSummaryRequest());
    };

    dispatch(requestHotelSummary());

    const api = new Request(dispatch, successFn, errorFn);

    const url = apiUrl.HOTEL_PERFORMANCE_SUMMARY
    .replace('{propertyId}', propertyId)
    .replace('{startDate}', startDate)
    .replace('{endDate}', endDate);
    return api.get(url);
  };
}

export function fetchHotelSummaryIfNeeded(propertyId, startDate, endDate) {
  return (dispatch, getState) => {
    if (shouldFetchHotelSummary(getState())) {
      return dispatch(fetchHotelSummary(propertyId, startDate, endDate));
    }
      // Let the calling code know there's nothing to wait for.
    return Promise.resolve();
  };
}

export function storeResetHotelSummary() {
  return (dispatch) => {
    dispatch({ type: actions.DASHBOARD_ITEMS_STORE_RESET });
  };
}
