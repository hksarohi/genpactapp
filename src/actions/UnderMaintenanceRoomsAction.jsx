import { actions, apiUrl } from './../constants/Constants';
import CommonUtils from './../utils/CommonUtils';
import Request from './../utils/Request';

export function requestUnderMaintenanceRoomsSummary() {
  return {
    type: actions.UNDER_MAINTENANCE_ROOMS_LOADING,
  };
}

export function receivedUnderMaintenanceRoomsSummary(roomsSummary) {
  return {
    type: actions.UNDER_MAINTENANCE_ROOMS_LOADED,
    payload: {
      receivedAt: Date.now(),
      underMaintenanceRoomsSummary: roomsSummary,
    },
  };
}

export function failedUnderMaintenanceRoomsSummaryRequest(error) {
  return {
    type: actions.UNDER_MAINTENANCE_ROOMS_LOADING_FAILED,
  };
}

export function shouldFetchUnderMaintenanceRoomsSummary(state) {
  return (CommonUtils.isEmpty(state.underMaintenanceRooms.underMaintenanceRoomsSummary));
}

export function fetchUnderMaintenanceRoomsSummary(propertyId, startDate, endDate) {
  return (dispatch) => {
    const successFn = (responseData) => {
      const underMaintenanceRoomsSummary = !CommonUtils.isEmpty(responseData) ? responseData : {};
      dispatch(receivedUnderMaintenanceRoomsSummary(underMaintenanceRoomsSummary));
    };

    const errorFn = (error) => {
      dispatch(failedUnderMaintenanceRoomsSummaryRequest(error));
    };

    dispatch(requestUnderMaintenanceRoomsSummary());

    const api = new Request(dispatch, successFn, errorFn);

    const url = apiUrl.UNDER_MAINTENANCE_ROOMS_SUMMARY
      .replace('{propertyId}', propertyId)
      .replace('{startDate}', startDate)
      .replace('{endDate}', endDate);
    return api.get(url);
  };
}

export function fetchUnderMaintenanceRoomsSummeryIfNeeded(propertyId, startDate, endDate) {
  return (dispatch, getState) => {
    if (shouldFetchUnderMaintenanceRoomsSummary(getState)) {
      return dispatch(fetchUnderMaintenanceRoomsSummary(propertyId, startDate, endDate));
    }

    return Promise.resolve();
  };
}

export function storeResetUnderMaintenanceRooms() {
  return (dispatch) => {
    dispatch({ type: actions.UNDER_MAINTENANCE_ROOMS_STORE_RESET });
  };
}
