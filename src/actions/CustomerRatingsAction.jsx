import { actions, apiUrl } from './../constants/Constants';
import CommonUtils from './../utils/CommonUtils';
import Request from './../utils/Request';

export function requestCustomerRatingsSummary() {
  return {
    type: actions.CUSTOMER_RATINGS_LOADING,
  };
}

export function receivedCustomerRatingsSummary(customerRatingsSummary) {
  return {
    type: actions.CUSTOMER_RATINGS_LOADED,
    payload: {
      receivedAt: Date.now(),
      customerRatingsSummary,
    },
  };
}

export function failedCustomerRatingsSummaryRequest(error) {
  return {
    type: actions.CUSTOMER_RATINGS_LOADING_FAILED,
  };
}

export function shouldFetchCustomerRatingsSummary(state) {
  return (CommonUtils.isEmpty(state.customerRatings.customerRatingsSummary));
}

export function fetchCustomerRatingsSummary(propertyId, startDate, endDate, page, limit) {
  return (dispatch) => {
    const successFn = (responseData) => {
      const customerRatingsSummary = !CommonUtils.isEmpty(responseData) ? responseData : {};
      dispatch(receivedCustomerRatingsSummary(customerRatingsSummary));
    };

    const errorFn = (error) => {
      dispatch(failedCustomerRatingsSummaryRequest(error));
    };

    dispatch(requestCustomerRatingsSummary());

    const api = new Request(dispatch, successFn, errorFn);

    const url = apiUrl.CUSTOMER_RATINGS_SUMMARY
      .replace('{propertyId}', propertyId)
      .replace('{startDate}', startDate)
      .replace('{endDate}', endDate)
      .replace('{page}', page)
      .replace('{limit}', limit);
    return api.get(url);
  };
}

export function fetchCustomerRatingsSummeryIfNeeded(propertyId, startDate, endDate, page, limit) {
  return (dispatch, getState) => {
    if (shouldFetchCustomerRatingsSummary(getState)) {
      return dispatch(fetchCustomerRatingsSummary(propertyId, startDate, endDate, page, limit));
    }

    return Promise.resolve();
  };
}

export function requestCustomerReviews() {
  return {
    type: actions.CUSTOMER_REVIEWS_LOADING,
  };
}

export function receivedCustomerReviews(customerReviews) {
  return {
    type: actions.CUSTOMER_REVIEWS_LOADED,
    payload: {
      receivedAt: Date.now(),
      customerReviews,
    },
  };
}

export function failedCustomerReviewsRequest(error) {
  return {
    type: actions.CUSTOMER_REVIEWS_LOADING_FAILED,
  };
}

export function shouldFetchCustomerReviews(state) {
  return (CommonUtils.isEmpty(state.customerRatings.customerRatingsSummary));
}

export function fetchCustomerReviews(propertyId, page, limit) {
  return (dispatch) => {
    const successFn = (responseData) => {
      const customerReviews = !CommonUtils.isEmpty(responseData) ? responseData : {};
      const content = CommonUtils.getObjectValueIfEmpty(customerReviews, 'content', []);

      if (content.length > 0) {
        dispatch(receivedCustomerReviews(customerReviews));
      } else {
        dispatch(failedCustomerReviewsRequest());
      }
    };

    const errorFn = (error) => {
      dispatch(failedCustomerReviewsRequest(error));
    };

    dispatch(requestCustomerReviews());

    const api = new Request(dispatch, successFn, errorFn);

    const url = apiUrl.CUSTOMER_REVIEWS
      .replace('{propertyId}', propertyId)
      .replace('{page}', page)
      .replace('{limit}', limit);
    return api.get(url);
  };
}

export function fetchCustomerReviewsIfNeeded(propertyId, page, limit) {
  return (dispatch, getState) => {
    if (shouldFetchCustomerReviews(getState)) {
      return dispatch(fetchCustomerReviews(propertyId, page, limit));
    }

    return Promise.resolve();
  };
}

export function storeResetCustomerRatings() {
  return (dispatch) => {
    dispatch({ type: actions.CUSTOMER_RATINGS_STORE_RESET });
  };
}
