import { actions, apiUrl } from './../constants/Constants';
import CommonUtils from './../utils/CommonUtils';
import Request from './../utils/Request';

export function requestRevenueReportSummary() {
  return {
    type: actions.REVENUE_REPORT_LOADING,
  };
}

export function receivedRevenueReportSummary(revenueReportSummary) {
  return {
    type: actions.REVENUE_REPORT_LOADED,
    payload: {
      receivedAt: Date.now(),
      revenueReportSummary,
    },
  };
}

export function failedRevenueReportSummaryRequest(error) {
  return {
    type: actions.REVENUE_REPORT_LOADING_FAILED,
  };
}

export function shouldFetchRevenueReportSummary(state) {
  return (CommonUtils.isEmpty(state.revenueReport.revenueReportSummary));
}

export function fetchRevenueReportSummary(propertyId, startDate, endDate) {
  return (dispatch) => {
    const successFn = (responseData) => {
      const RevenueReportSummary = !CommonUtils.isEmpty(responseData) ? responseData : {};

      dispatch(receivedRevenueReportSummary(RevenueReportSummary));
    };

    const errorFn = (error) => {
      dispatch(failedRevenueReportSummaryRequest(error));
    };

    dispatch(requestRevenueReportSummary());

    const api = new Request(dispatch, successFn, errorFn);

    const url = apiUrl.REVENUE_REPORT_SUMMARY
    .replace('{propertyId}', propertyId)
    .replace('{startDate}', startDate)
    .replace('{endDate}', endDate);
    return api.get(url);
  };
}

export function fetchRevenueReportSummeryIfNeeded(propertyId, startDate, endDate) {
  return (dispatch, getState) => {
    if (shouldFetchRevenueReportSummary(getState)) {
      return dispatch(fetchRevenueReportSummary(propertyId, startDate, endDate));
    }

    return Promise.resolve();
  };
}

export function storeResetRevenueReportSummary() {
  return (dispatch) => {
    dispatch({ type: actions.REVENUE_REPORT_STORE_RESET });
  };
}
