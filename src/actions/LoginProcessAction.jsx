import { actions, apiUrl } from './../constants/Constants';
import CommonUtils from './../utils/CommonUtils';
import Request from './../utils/Request';

/**
 * Login Actions
 */

export function loginResetStore() {
  return {
    type: actions.LOGIN_RESET_STORE,
  };
}

export function loginInit() {
  return {
    type: actions.LOGIN_REQUEST_INIT,
  };
}

export function loginSuccessful(data, headers, params) {
  return {
    type: actions.LOGIN_REQUEST_SUCCESS,
    payload: {
      receivedAt: Date.now(),
      data,
      headers,
      params,
    },
  };
}

export function loginFailed(error) {
  return {
    type: actions.LOGIN_REQUEST_FAILED,
  };
}

export function loginRequest(params) {
  return (dispatch) => {
    const successFn = (data, headers) => {
      dispatch(loginSuccessful(data, headers, params));
    };

    const errorFn = (error) => {
      dispatch(loginFailed(error));
    };

    dispatch(loginInit());

    const api = new Request(dispatch, successFn, errorFn, false);
    return api.post(apiUrl.LOGIN_HOTELIER, params);
  };
}


/**
 * Validate Otp Actions
 */
export function loginOtpValidationInit() {
  return {
    type: actions.LOGIN_OTP_VALIDATION_INIT,
  };
}

export function loginOtpValidationSuccessful(data, headers) {
  return {
    type: actions.LOGIN_OTP_VALIDATION_SUCCESS,
    payload: {
      receivedAt: Date.now(),
      data,
      headers,
    },
  };
}

export function loginOtpValidationFailed(error) {
  return {
    type: actions.LOGIN_OTP_VALIDATION_FAILED,
  };
}

export function loginOtpValidationRequest(params) {
  return (dispatch) => {
    const successFn = (data, headers) => {
      dispatch(loginOtpValidationSuccessful(data, headers));
    };

    const errorFn = (error) => {
      dispatch(loginOtpValidationFailed(error));
    };

    dispatch(loginOtpValidationInit());

    const api = new Request(dispatch, successFn, errorFn, false);
    return api.post(apiUrl.LOGIN_OTP_VALIDATION, params);
  };
}


/**
 * Resend Otp Actions
 */
export function loginOtpResendInit() {
  return {
    type: actions.LOGIN_OTP_RESEND_INIT,
  };
}

export function loginOtpResendSuccessful(data) {
  return {
    type: actions.LOGIN_OTP_RESEND_SUCCESS,
    payload: {
      receivedAt: Date.now(),
      data,
    },
  };
}

export function loginOtpResendFailed(error) {
  return {
    type: actions.LOGIN_OTP_RESEND_FAILED,
  };
}

export function loginOtpResendRequest(params) {
  return (dispatch) => {
    const successFn = (data) => {
      dispatch(loginOtpResendSuccessful(data));
    };

    const errorFn = (error) => {
      dispatch(loginOtpResendFailed(error));
    };

    dispatch(loginOtpResendInit());

    const api = new Request(dispatch, successFn, errorFn, false);
    return api.post(apiUrl.LOGIN_OTP_RESEND, params);
  };
}


/**
 * Get User Profile Actions
 */
export function fetchUserProfileInit() {
  return {
    type: actions.USER_PROFILE_FETCH_REQUEST_INIT,
  };
}

export function fetchUserProfileSuccessful(data) {
  return {
    type: actions.USER_PROFILE_FETCH_REQUEST_SUCCESS,
    payload: {
      receivedAt: Date.now(),
      data,
    },
  };
}

export function logoutFailed(error) {
  return {
    type: actions.LOGOUT_REQUEST,
  };
}

export function logoutSuccessful() {
  return {
    type: actions.LOGOUT_REQUEST,
  };
}

export function logoutUser() {
  return (dispatch) => {
    const successFn = (data, headers) => {
      dispatch(logoutSuccessful(data, headers));
    };

    const errorFn = (error) => {
      dispatch(logoutFailed(error));
    };

    const api = new Request(dispatch, successFn, errorFn);
    return api.get(apiUrl.LOGOUT_HOTELIER);
  };
}

export function fetchUserProfileFailed() {
  return {
    type: actions.USER_PROFILE_FETCH_REQUEST_FAILED,
  };
}

export function fetchUserProfileRequest(params) {
  return (dispatch) => {
    const successFn = (data) => {
      dispatch(fetchUserProfileSuccessful(data));
    };

    const errorFn = (data, headers, status) => {
      dispatch(fetchUserProfileFailed());
    };

    dispatch(fetchUserProfileInit());

    const api = new Request(dispatch, successFn, errorFn);
    return api.get(apiUrl.FETCH_USER_PROFILE, params);
  };
}

export function shouldFetchUserProfile(state) {
  return (CommonUtils.isEmpty(state.loginUser.userData));
}

export function fetchUserProfileIfNeeded(params) {
  return (dispatch, getState) => {
    if (shouldFetchUserProfile(getState())) {
      return dispatch(fetchUserProfileRequest(params));
    }
    // Let the calling code know there's nothing to wait for.
    return Promise.resolve();
  };
}
