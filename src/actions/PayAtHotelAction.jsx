import { actions, apiUrl } from './../constants/Constants';
import CommonUtils from './../utils/CommonUtils';
import Request from './../utils/Request';
import PayAtHotelHelper from './../helpers/PayAtHotelHelper';

export function requestPayAtHotelSummary() {
  return {
    type: actions.PAY_AT_HOTEL_LOADING,
  };
}

export function receivedPayAtHotelSummary(roomsSummary) {
  return {
    type: actions.PAY_AT_HOTEL_LOADED,
    payload: {
      receivedAt: Date.now(),
      payAtHotelSummary: PayAtHotelHelper.getStatementItems(roomsSummary),
    },
  };
}

export function failedPayAtHotelSummaryRequest(error) {
  return {
    type: actions.PAY_AT_HOTEL_LOADING_FAILED,
  };
}

export function shouldFetchPayAtHotelSummary(state) {
  return (CommonUtils.isEmpty(state.payAtHotel.payAtHotelSummary));
}

export function fetchPayAtHotelSummary(propertyId, startDate, endDate) {
  return (dispatch) => {
    const successFn = (responseData) => {
      const summary = !CommonUtils.isEmpty(responseData) ? responseData : {};
      dispatch(receivedPayAtHotelSummary(summary));
    };

    const errorFn = (error) => {
      dispatch(failedPayAtHotelSummaryRequest(error));
    };

    dispatch(requestPayAtHotelSummary());

    const api = new Request(dispatch, successFn, errorFn);

    const url = apiUrl.PAY_AT_HOTEL_SUMMARY
    .replace('{propertyId}', propertyId)
    .replace('{startDate}', startDate)
    .replace('{endDate}', endDate);
    return api.get(url);
  };
}

export function fetchPayAtHotelSummeryIfNeeded(propertyId, startDate, endDate) {
  return (dispatch, getState) => {
    if (shouldFetchPayAtHotelSummary(getState)) {
      return dispatch(fetchPayAtHotelSummary(propertyId, startDate, endDate));
    }

    return Promise.resolve();
  };
}

export function storePayAtHotelSummary() {
  return (dispatch) => {
    dispatch({ type: actions.PAY_AT_HOTEL_SUMMARY_STORE_RESET });
  };
}
