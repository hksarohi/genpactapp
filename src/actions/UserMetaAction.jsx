import { actions, apiUrl } from './../constants/Constants';
import CommonUtils from './../utils/CommonUtils';
import CommonHelper from './../helpers/CommonHelper';
import Request from './../utils/Request';

export function selectOwnerHotel(hoteDetails) {
  return {
    type: actions.OWNER_CHANGED_SELECTED_HOTEL,
    payload: {
      hotel: { ...hoteDetails },
    },
  };
}

export function requestOwnerHotels() {
  return {
    type: actions.OWNER_HOTELS_LOADING,
  };
}

export function failedOwnerHotelsRequest() {
  return {
    type: actions.OWNER_HOTELS_LOADING_FAILED,
  };
}

export function receivedOwnerHotels(hotels) {
  return {
    type: actions.OWNER_HOTELS_LOADED,
    payload: {
      receivedAt: Date.now(),
      hotels,
    },
  };
}

export function shouldFetchOwnerHotels(state) {
  return (state.userMeta.hotels.length === 0);
}

export function fetchOwnerHotels() {
  return (dispatch) => {
    const successFn = (responseData) => {
      const hotels = !CommonUtils.isEmpty(responseData) ? responseData : [];

      dispatch(receivedOwnerHotels(hotels));

      // find the already selected hotel
      let selectHotel = CommonUtils.getObjectValueIfEmpty(hotels, '0', null);
      const storageHotelId = CommonHelper.getStorageSelectedHotel();

      if (!CommonUtils.isEmpty(storageHotelId)) {
        for (let i = 0; i < hotels.length; i += 1) {
          const theHotelId = CommonUtils.getObjectValue(hotels[i], 'propertyId');

          if (parseInt(theHotelId, 10) === parseInt(storageHotelId, 10)) {
            selectHotel = { ...hotels[i] };
            break;
          }
        }
      }

      dispatch(selectOwnerHotel(selectHotel));
    };

    const errorFn = () => {
      dispatch(failedOwnerHotelsRequest());
    };

    dispatch(requestOwnerHotels());

    const api = new Request(dispatch, successFn, errorFn, true, dispatch);
    return api.get(apiUrl.PROPERTY_LIST);
  };
}

export function fetchOwnerHotelsIfNeeded() {
  return (dispatch, getState) => {
    if (shouldFetchOwnerHotels(getState())) {
      // Dispatch a thunk from thunk!
      return dispatch(fetchOwnerHotels());
    }
      // Let the calling code know there's nothing to wait for.
    return Promise.resolve();
  };
}
