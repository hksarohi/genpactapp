import { actions, apiUrl } from './../constants/Constants';
import CommonUtils from './../utils/CommonUtils';
import Request from './../utils/Request';

export function requestOccupancyReportSummary() {
  return {
    type: actions.OCCUPANCY_REPORT_LOADING,
  };
}

export function receivedOccupancyReportSummary(occupancyReportSummary) {
  return {
    type: actions.OCCUPANCY_REPORT_LOADED,
    payload: {
      receivedAt: Date.now(),
      occupancyReportSummary,
    },
  };
}

export function failedOccupancyReportSummaryRequest(error) {
  return {
    type: actions.OCCUPANCY_REPORT_LOADING_FAILED,
  };
}

export function shouldFetchOccupancyReportSummary(state) {
  return (CommonUtils.isEmpty(state.occupancyReport.occupancyReportSummary));
}

export function fetchOccupancyReportSummary(propertyId, startDate, endDate) {
  return (dispatch) => {
    const successFn = (responseData) => {
      const occupancyReportSummary = !CommonUtils.isEmpty(responseData) ? responseData : {};

      dispatch(receivedOccupancyReportSummary(occupancyReportSummary));
    };

    const errorFn = (error) => {
      dispatch(failedOccupancyReportSummaryRequest(error));
    };

    dispatch(requestOccupancyReportSummary());

    const api = new Request(dispatch, successFn, errorFn);

    const url = apiUrl.OCCUPANCY_REPORT_SUMMARY
      .replace('{propertyId}', propertyId)
      .replace('{startDate}', startDate)
      .replace('{endDate}', endDate);
    return api.get(url);
  };
}

export function fetchOccupancyReportSummeryIfNeeded(propertyId, startDate, endDate) {
  return (dispatch, getState) => {
    if (shouldFetchOccupancyReportSummary(getState)) {
      return dispatch(fetchOccupancyReportSummary(propertyId, startDate, endDate));
    }

    return Promise.resolve();
  };
}

export function storeResetOccupancyReportSummary() {
  return (dispatch) => {
    dispatch({ type: actions.OCCUPANCY_REPORT_STORE_RESET });
  };
}
