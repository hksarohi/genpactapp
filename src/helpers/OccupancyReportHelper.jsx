import CommonUtils from './../utils/CommonUtils';
import Formatter from './Formatter';

class OccupancyReportHelper {
  static getOccupancyReportItems(occupancyReport) {
    const average = Formatter.formatPercent(Formatter.formatRating(CommonUtils.getObjectValue(occupancyReport, 'average', '--'), 2));
    const dayWiseData = CommonUtils.getObjectValue(occupancyReport, 'dayWiseData', []);

    const occupancyReportItems = {
      items: {
        average,
        dayWiseData,
      },
    };

    return occupancyReportItems;
  }
}

export default OccupancyReportHelper;
