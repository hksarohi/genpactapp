import CommonUtils from './../utils/CommonUtils';

class AuthenticationHelper {
  static saveAuthToken(token) {
    localStorage.setItem('FabAuthToken', token);
  }

  static getAuthToken() {
    return localStorage.getItem('FabAuthToken');
  }

  static clearAuthToken() {
    localStorage.removeItem('FabAuthToken');
  }

  static isAuthenticated() {
    const isAuthenticated = !CommonUtils.isEmpty(this.getAuthToken());
    return isAuthenticated;
  }

  /**
   * Save mobile number to local storage 
   * @param {*} mobile 
   */
  static saveMobileNumber(mobile) {
    localStorage.setItem('mobile', mobile);
  }

  /**
   * Get mobile number from local storage 
   */
  static getMobileNumber() {
    return localStorage.getItem('mobile');
  }
}

export default AuthenticationHelper;
