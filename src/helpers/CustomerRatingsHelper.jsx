import CommonUtils from './../utils/CommonUtils';
import Formatter from './Formatter';

class CustomerRatingsHelper {
  static getFabHotelRatings(customerRatingsSummary) {
    const overallFabHotelRating = Formatter.formatRating(CommonUtils.getObjectValue(customerRatingsSummary, 'fabHotelReview', '--'));
    const overallHotelPercentile = Formatter.formatPercent(Formatter.formatAmount(CommonUtils.getObjectValue(customerRatingsSummary, 'percentageOfHotelBeforeYou', '--')));

    const hotelRatings = {
      heading: 'FabHotels Ratings',
      items: [
        {
          title: 'Overall FabHotels Rating',
          count: overallFabHotelRating,
          subtext: 'out of 5',
        },
        {
          title: 'Percentile',
          count: overallHotelPercentile,
          subtext: 'doing better',
          showLogo: true,
        },
      ],
    };

    return hotelRatings;
  }

  static getSelectedHotelRatings(customerRatingsSummary) {
    const overallHotelRating = Formatter.formatRating(CommonUtils.getObjectValue(customerRatingsSummary, 'hotelReview', '--'));
    const weeklyHotelRating = Formatter.formatRating(CommonUtils.getObjectValue(customerRatingsSummary, 'hotelRecentReview', '--'));

    const fabHotelRatings = {
      heading: 'Ratings',
      items: [
        {
          title: 'Last 7 days Rating',
          count: weeklyHotelRating,
          subtext: 'out of 5',
        },
        {
          title: 'Overall Hotel Rating',
          count: overallHotelRating,
          subtext: 'out of 5',
        },
      ],
    };

    return fabHotelRatings;
  }

  static getCustomerRatingsList(customerRatingsSummary) {
    const customerReviewsCount = CommonUtils.getObjectValue(customerRatingsSummary, 'pageReviews.totalElements', 0);
    const customerReviews = CommonUtils.getObjectValue(customerRatingsSummary, 'pageReviews.content', []);

    const customerRatingsItems = {
      heading: 'Reviews',
      count: customerReviewsCount,
      items: customerReviews,
    };

    return customerRatingsItems;
  }
}

export default CustomerRatingsHelper;
