import CommonUtils from './../utils/CommonUtils';
import Formatter from './Formatter';

class DashboardHelper {
  static getMenuList(hotelSummary, selectedHotel) {
    const isMgProperty = CommonUtils.getObjectValue(selectedHotel, 'mgProperty', false);

    const hotelRating = Formatter.formatRating(CommonUtils.getObjectValue(hotelSummary, 'hotelReview', '--')).toString();
    const underMaintenance = Formatter.formatCount(CommonUtils.getObjectValue(hotelSummary, 'outOfOrderRoom', '--')).toString();
    // const debitNotes = Formatter.formatCount(CommonUtils.getObjectValue(hotelSummary, 'debitNotes', '--')).toString();
    // const openIssues = Formatter.formatCount(CommonUtils.getObjectValue(hotelSummary, 'openIssues', '--')).toString();

    const avgDailyRevenue = Formatter.formatCount(Formatter.formatAmount(CommonUtils.getObjectValue(hotelSummary, 'averageDailyRevenue', '--'))).toString();
    const avgDailyBooking = Formatter.formatPercent(Formatter.formatAmount(CommonUtils.getObjectValue(hotelSummary, 'averageDailyBooking', '--'))).toString();
    // const avgPayAtHotel = Formatter.formatPercent(Formatter.formatAmount(CommonUtils.getObjectValue(hotelSummary, 'percentageOfPayAtHotelBooking', '--'))).toString();

    const menuCardList = [
      {
        menuCardHeading: "Let's delight guests !",
        menuCardHeadingIcon: 'delight-guest',
        menuItemList: [
          {
            ratingCount: hotelRating,
            itemHeading: 'Customer Ratings (out of 5)',
            navigateTo: '/customer-ratings',
            itemRatingClass: (!isNaN(hotelRating) && hotelRating > 3) ? 'customer-rating-overall' : '',
            isItemEnabled: true,
          },
          {
            ratingCount: underMaintenance,
            itemHeading: 'Under Maintenance Rooms',
            navigateTo: '/under-maintenance-rooms',
            itemRatingClass: (!isNaN(underMaintenance) && underMaintenance < 1) ? 'customer-rating-overall' : '',
            isItemEnabled: true,
          },
          {
            ratingCount: '',
            itemHeading: 'Debit Notes',
            navigateTo: '/debit-notes',
            isItemEnabled: false,
          },
          {
            ratingCount: '',
            itemHeading: 'Open Issues',
            navigateTo: '/open-issues',
            isItemEnabled: false,
          },
        ],
      },

      {
        menuCardHeading: "Let's generate higher returns !",
        menuCardHeadingIcon: 'more-benifits',
        menuItemList: [
          {
            ratingCount: avgDailyRevenue,
            itemHeading: 'ADR incl. of Taxes',
            navigateTo: '/average-revenue',
            itemRatingClass: 'customer-percentage',
            isItemEnabled: !isMgProperty,
          },
          {
            ratingCount: avgDailyBooking,
            itemHeading: 'Occupancy (last 7 days)',
            navigateTo: '/occupancy-report',
            itemRatingClass: 'customer-percentage',
            isItemEnabled: true,
          },
          {
            ratingCount: '',
            itemHeading: 'Pay @ Hotel (last 7 days)',
            navigateTo: '/pay-at-hotel',
            itemRatingClass: 'customer-payment',
            isItemEnabled: true,
          },
          {
            itemHeading: 'Invoices and Payment Status',
            navigateTo: '/invoices',
            itemHeadingClass: 'customer-payment',
            isItemEnabled: false,
          },
        ],
      },
    ];

    return menuCardList;
  }
}

export default DashboardHelper;
