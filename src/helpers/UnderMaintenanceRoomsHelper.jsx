import CommonUtils from './../utils/CommonUtils';
import Formatter from './Formatter';

class UnderMaintenanceRoomsHelper {
  static getRoomsList(todaySummary, yesterdaySummary) {
    const todayRooms = Formatter.formatCount(CommonUtils.getObjectValue(todaySummary, 'rooms', '--'));
    const yesterdayRooms = Formatter.formatCount(CommonUtils.getObjectValue(yesterdaySummary, 'rooms', '--'));

    const underMaintenanceRoomsItems = {
      roomsInfo: {
        heading: 'No. of Rooms Under maintenance ',
        items: [
          {
            title: 'Today',
            count: todayRooms,
            subtext: todayRooms > 1 ? 'Rooms' : 'Room',
          },
          {
            title: 'Yesterday',
            count: yesterdayRooms,
            subtext: yesterdayRooms > 1 ? 'Rooms' : 'Room',
          },
        ],
      },
    };

    return underMaintenanceRoomsItems;
  }
}

export default UnderMaintenanceRoomsHelper;
