import CommonUtils from './../utils/CommonUtils';
import Formatter from './Formatter';

class RevenueReportHelper {
  static getRevenueReportItems(revenueReport) {
    const average = Formatter.formatAmount(CommonUtils.getObjectValue(revenueReport, 'average', '--'));
    const dayWiseData = CommonUtils.getObjectValue(revenueReport, 'dayWiseData', []);

    const revenueReportItems = {
      items: {
        average,
        dayWiseData,
      },
    };

    return revenueReportItems;
  }
}

export default RevenueReportHelper;
