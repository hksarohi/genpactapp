import React from 'react';
import CommonUtils from './../utils/CommonUtils';
import DateUtils from './../utils/DateUtils';
import Formatter from './Formatter';

class PayAtHotelHelper {
  static getStatementItems(details) {
    const items = [];

    if (!CommonUtils.isEmpty(details)) {
      Object.keys(details).forEach((reportDate) => {
        if (CommonUtils.keyExists(reportDate, details)) {
          const rowData = CommonUtils.getObjectValueIfEmpty(details, reportDate, {});
          rowData.reportDate = reportDate;

          items.push(rowData);
        }
      });
    }

    return items;
  }

  static getInfoTableHeadings() {
    return ['Date', 'Amount to be collected'];
  }

  static getInfoTableValueKeys() {
    return ['reportDate', 'payAtHotelAmount'];
  }

  static formatInfoTableRowValue(key, item) {
    switch (key) {
      case 'reportDate':
        return DateUtils.getDateInFormat(item, DateUtils.dMY, ' ');

      case 'payAtHotelAmount':
        return (<span>&#x20B9; {Formatter.priceWithCommas(Formatter.formatAmount(item))}</span>);

      default:
        break;
    }

    return item;
  }
}

export default PayAtHotelHelper;
