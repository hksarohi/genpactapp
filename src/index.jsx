/**
 | Core Components
 */
import React from 'react';
import ReactDOM from 'react-dom';
import thunk from 'redux-thunk';
import { createLogger } from 'redux-logger';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { Route, Switch, Redirect, BrowserRouter } from 'react-router-dom';
import createBrowserHistory from 'history/createBrowserHistory';
import { ConnectedRouter, routerMiddleware } from 'react-router-redux';

/**
 | Custom Components
 */
import LoginAuthorization from './containers/LoginAuthorization';
import Login from './containers/LoginPage';
import Properties from './containers/PropertiesPage';
import Dashboard from './containers/DashboardPage';
import CustomerRatings from './containers/CustomerRatingsPage';
import UnderMaintenanceRooms from './containers/UnderMaintenanceRoomsPage';
import AverageRevenue from './containers/RevenueReportPage';
import OccupancyReport from './containers/OccupancyReportPage';
import PayAtHotel from './containers/PayAtHotelPage';
import StaticPage from './containers/StaticPage';

/**
 * Helpers
 */
import AuthenticationHelper from './helpers/AuthenticationHelper';
import CommonHelper from './helpers/CommonHelper';

/**
 | Reducers
 */
import allReducers from './reducers';

/**
 | Constants 
 */
const isDevelopmentMode = (process.env.NODE_ENV !== 'production');

/**
 | Stylesheets
 */
require('../assets/style/style.less');

/**
 | Logger
 */
const logger = createLogger();

/**
 | History
 */
const history = createBrowserHistory();

/**
 | Middleware
 */
const middleware = routerMiddleware(history);

/**
 | Store
 */
let store = null;
if (isDevelopmentMode) {
  store = createStore(
    allReducers,
    applyMiddleware(middleware, logger, thunk),
  );
} else {
  store = createStore(
    allReducers,
    applyMiddleware(middleware, thunk),
  );
}

// Now you can dispatch navigation actions from anywhere!
// store.dispatch(push('/foo'))

/**
 * Auth Check
 * requireAuth could not be used for '/login' path because a route has to be defined for all paths
 * requireAuth uses './login' redirect hence added a Route for '/login' in beforeLogin
 */
function requireAuth() {
  let route = (<Redirect to="/login" />);

  if (AuthenticationHelper.isAuthenticated()) {
    route = CommonHelper.isHotelSelected() ? (<Redirect to="/dashboard" />) : (<Redirect to="/properties" />);
  }

  return route;
}

/**
 * Before Login
 * requireAuth could not be used for '/login' path because a route has to be defined for all paths
 * requireAuth uses './login' redirect hence added a Route for '/login' in beforeLogin
 */
function beforeLogin() {
  let route = (<Route path="/login" component={Login} />);

  if (AuthenticationHelper.isAuthenticated()) {
    route = CommonHelper.isHotelSelected() ? (<Redirect to="/dashboard" />) : (<Redirect to="/properties" />);
  }

  return route;
}

/**
 | Render
 */
ReactDOM.render(
  <Provider store={store}>
    { /* ConnectedRouter will use the store from Provider automatically */ }
    <ConnectedRouter history={history}>
      <BrowserRouter basename="/partners/">
        <Switch>
          <Route path="/" exact render={requireAuth} />
          <Route path="/login" render={beforeLogin} />
          <LoginAuthorization path="/properties" component={Properties} />
          <LoginAuthorization path="/dashboard" component={Dashboard} />
          <LoginAuthorization path="/customer-ratings" component={CustomerRatings} />
          <LoginAuthorization path="/under-maintenance-rooms" component={UnderMaintenanceRooms} />
          <LoginAuthorization path="/average-revenue" component={AverageRevenue} />
          <LoginAuthorization path="/pay-at-hotel" component={PayAtHotel} />
          <LoginAuthorization path="/occupancy-report" component={OccupancyReport} />
          <LoginAuthorization path="/help" component={StaticPage} />
        </Switch>
      </BrowserRouter>
    </ConnectedRouter>
  </Provider>,
  document.getElementById('root'));
