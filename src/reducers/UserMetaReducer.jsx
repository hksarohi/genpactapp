import { actions } from './../constants/Constants';
import CommonUtils from './../utils/CommonUtils';

const initialState = {
  isHotelsLoading: false,
  hotels: [],
};

export default (state = initialState, action = {}) => {
  let changes = {};

  switch (action.type) {

    case actions.LOGOUT_REQUEST:
      changes = { ...initialState };
      break;

    case actions.LOGIN_RESET_STORE:
      changes = { ...initialState };
      break;

    case actions.OWNER_HOTELS_LOADING:
      changes = {
        isHotelsLoading: true,
        hotels: [],
      };
      break;

    case actions.OWNER_HOTELS_LOADED:
      changes = {
        isHotelsLoading: false,
        hotels: CommonUtils.getObjectValueIfEmpty(action, 'payload.hotels', []),
      };
      break;

    case actions.OWNER_HOTELS_LOADING_FAILED:
      changes = {
        isHotelsLoading: false,
        hotels: [],
      };
      break;

    default:
      changes = {};
      break;
  }

  return CommonUtils.clone(state, changes);
};
