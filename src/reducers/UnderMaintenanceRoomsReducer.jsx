import { actions } from './../constants/Constants';
import CommonUtils from './../utils/CommonUtils';
import DateUtils from './../utils/DateUtils';

const initialState = {
  isUnderMaintenanceRoomsLoading: false,
  underMaintenanceRoomsSummary: null,
  todayOutOfOrderSummary: null,
  yesterdayOutOfOrderSummary: null,
};

export default (state = initialState, action = {}) => {
  let changes = {};

  switch (action.type) {
    case actions.UNDER_MAINTENANCE_ROOMS_STORE_RESET:
      changes = { ...initialState };
      break;

    case actions.LOGOUT_REQUEST:
      changes = { ...initialState };
      break;

    case actions.LOGIN_RESET_STORE:
      changes = { ...initialState };
      break;

    case actions.OWNER_CHANGED_SELECTED_HOTEL:
      changes = { ...initialState };
      break;

    case actions.UNDER_MAINTENANCE_ROOMS_LOADING:
      changes = {
        isUnderMaintenanceRoomsLoading: true,
        underMaintenanceRoomsSummary: null,
      };
      break;

    case actions.UNDER_MAINTENANCE_ROOMS_LOADED: {
      const oooSummary = CommonUtils.getObjectValueIfEmpty(action, 'payload.underMaintenanceRoomsSummary', {});
      changes = {
        isUnderMaintenanceRoomsLoading: false,
        underMaintenanceRoomsSummary: oooSummary,
      };

      const today = DateUtils.getDateInFormat(new Date(), DateUtils.yyyymmdd);
      const yesterday = DateUtils.getDateInFormat(DateUtils.add(today, { days: -1 }), DateUtils.yyyymmdd);
      const tSummary = CommonUtils.getObjectValueIfEmpty(oooSummary, today, {});
      const ySummary = CommonUtils.getObjectValueIfEmpty(oooSummary, yesterday, {});

      if (!CommonUtils.isEmpty(tSummary)) changes.todayOutOfOrderSummary = tSummary;
      if (!CommonUtils.isEmpty(ySummary)) changes.yesterdayOutOfOrderSummary = ySummary;

      /**
       * By Default today and yesterday under maintenance rooms will be 0
       */
      const todayAlreadyRooms = CommonUtils.getObjectValueIfEmpty(state, 'todayOutOfOrderSummary.rooms', 0);
      const yesterdayAlreadyRooms = CommonUtils.getObjectValueIfEmpty(state, 'yesterdayOutOfOrderSummary.rooms', 0);

      if (todayAlreadyRooms <= 0 && CommonUtils.isEmpty(changes.todayOutOfOrderSummary)) {
        changes.todayOutOfOrderSummary = { rooms: 0 };
      }

      if (yesterdayAlreadyRooms <= 0 && CommonUtils.isEmpty(changes.yesterdayOutOfOrderSummary)) {
        changes.yesterdayOutOfOrderSummary = { rooms: 0 };
      }
    }
      break;

    case actions.UNDER_MAINTENANCE_ROOMS_LOADING_FAILED:
      changes = {
        isUnderMaintenanceRoomsLoading: false,
        underMaintenanceRoomsSummary: null,
      };
      break;

    default:
      changes = {};
      break;
  }

  return CommonUtils.clone(state, changes);
};

