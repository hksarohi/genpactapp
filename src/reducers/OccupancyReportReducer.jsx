import { actions } from './../constants/Constants';
import CommonUtils from './../utils/CommonUtils';

const initialState = {
  isSummaryLoading: false,
  occupancyReportSummary: null,
};

export default (state = initialState, action = {}) => {
  let changes = {};

  switch (action.type) {
    case actions.OCCUPANCY_REPORT_STORE_RESET:
      changes = { ...initialState };
      break;

    case actions.LOGOUT_REQUEST:
      changes = { ...initialState };
      break;

    case actions.LOGIN_RESET_STORE:
      changes = { ...initialState };
      break;

    case actions.OWNER_CHANGED_SELECTED_HOTEL:
      changes = { ...initialState };
      break;

    case actions.OCCUPANCY_REPORT_LOADING:
      changes = {
        isSummaryLoading: true,
        occupancyReportSummary: null,
      };
      break;

    case actions.OCCUPANCY_REPORT_LOADED:
      changes = {
        isSummaryLoading: false,
        occupancyReportSummary: CommonUtils.getObjectValueIfEmpty(action, 'payload.occupancyReportSummary', []),
      };
      break;

    case actions.OCCUPANCY_REPORT_LOADING_FAILED:
      changes = {
        isSummaryLoading: false,
        occupancyReportSummary: null,
      };
      break;

    default:
      changes = {};
      break;
  }

  return CommonUtils.clone(state, changes);
};
