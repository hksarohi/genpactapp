import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import LoginUser from './../reducers/LoginProcessReducer';
import DashboardReducer from './../reducers/DashboardReducer';
import UserMetaReducer from './../reducers/UserMetaReducer';
import UserInputsReducer from './../reducers/UserInputsReducer';
import CustomerRatingsReducer from './../reducers/CustomerRatingsReducer';
import UnderMaintenanceRoomsReducer from './../reducers/UnderMaintenanceRoomsReducer';
import RevenueReportReducer from './../reducers/RevenueReportReducer';
import OccupancyReportReducer from './../reducers/OccupancyReportReducer';
import PayAtHotelReducer from './../reducers/PayAtHotelReducer';

const allReducers = combineReducers({
  routing: routerReducer,
  loginUser: LoginUser,
  dashboard: DashboardReducer,
  userMeta: UserMetaReducer,
  userInputs: UserInputsReducer,
  customerRatings: CustomerRatingsReducer,
  underMaintenanceRooms: UnderMaintenanceRoomsReducer,
  revenueReport: RevenueReportReducer,
  occupancyReport: OccupancyReportReducer,
  payAtHotel: PayAtHotelReducer,
});

export default allReducers;
