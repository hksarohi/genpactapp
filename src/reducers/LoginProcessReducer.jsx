import { actions } from './../constants/Constants';
import CommonUtils from './../utils/CommonUtils';
import AuthenticationHelper from './../helpers/AuthenticationHelper';

const initialState = {
  /*
  * TODO- isAuthenticated and user data need to be fetched from local storage
  */
  isLoggingIn: false,
  isLoggedIn: false,
  isOtpValidating: false,
  isOtpValidated: false,
  isResendingOtp: false,
  invalidOtpCount: 0,
  isAuthenticated: AuthenticationHelper.isAuthenticated(),
  isFetchingUserProfile: false,
  isFetchedUserProfile: false,
  userData: null,
};

export default (state = initialState, action = {}) => {
  let changes = {};

  switch (action.type) {
    case actions.LOGIN_RESET_STORE:
      changes = { ...initialState, isAuthenticated: AuthenticationHelper.isAuthenticated(), userData: state.userData };
      break;

    case actions.LOGIN_REQUEST_INIT:
      changes = {
        isLoggingIn: true,
        isLoggedIn: false,
        isOtpValidating: false,
        isOtpValidated: false,
        invalidOtpCount: 0,
        isAuthenticated: false,
        userData: null,
      };
      break;

    case actions.LOGIN_REQUEST_SUCCESS: {
      const userData = CommonUtils.getObjectValueIfEmpty(action, 'payload.params', {});

      changes = {
        isLoggingIn: false,
        isOtpValidated: false,
        invalidOtpCount: 0,
        isAuthenticated: false,
        isLoggedIn: CommonUtils.getObjectValueIfEmpty(action, 'payload.data.success', false),
        userData,
      };
    }
      break;

    case actions.LOGIN_REQUEST_FAILED:
      changes = {
        isLoggingIn: false,
        isLoggedIn: false,
        isOtpValidated: false,
        invalidOtpCount: 0,
        isAuthenticated: false,
        userData: null,
      };
      break;

    case actions.LOGIN_OTP_VALIDATION_INIT:
      changes = {
        isLoggingIn: false,
        isOtpValidating: true,
        isOtpValidated: false,
        isAuthenticated: false,
      };
      break;

    case actions.LOGIN_OTP_VALIDATION_SUCCESS: {
      const isOtpValidated = CommonUtils.getObjectValueIfEmpty(action, 'payload.data.success', false);
      const userData = isOtpValidated ? CommonUtils.getObjectValueIfEmpty(action, 'payload.data.data', {}) : state.userData;
      const mobile = isOtpValidated ? CommonUtils.getObjectValueIfEmpty(userData, 'mobile', '') : '';
      const invalidOtpCount = isOtpValidated ? 0 : (state.invalidOtpCount + 1);
      const authToken = CommonUtils.getObjectValueIfEmpty(action, 'payload.headers.token', null);
      const isAuthenticated = (isOtpValidated && !CommonUtils.isEmpty(userData) && !CommonUtils.isEmpty(authToken));

      if (isAuthenticated) AuthenticationHelper.saveAuthToken(authToken);
      if (isAuthenticated) AuthenticationHelper.saveMobileNumber(mobile);

      changes = {
        isOtpValidating: false,
        isAuthenticated,
        isOtpValidated,
        invalidOtpCount,
        userData,
      };
    }
      break;

    case actions.LOGIN_OTP_VALIDATION_FAILED:
      changes = {
        isOtpValidating: false,
        isOtpValidated: false,
        invalidOtpCount: (state.invalidOtpCount + 1),
        isAuthenticated: false,
      };
      break;

    case actions.LOGIN_OTP_RESEND_INIT:
      changes = {
        isResendingOtp: true,
      };
      break;

    case actions.LOGIN_OTP_RESEND_SUCCESS:
      changes = {
        isResendingOtp: false,
      };
      break;

    case actions.LOGIN_OTP_RESEND_FAILED:
      changes = {
        isResendingOtp: false,
      };
      break;

    case actions.USER_PROFILE_FETCH_REQUEST_INIT:
      changes = {
        isFetchingUserProfile: true,
        isFetchedUserProfile: false,
        userData: null,
      };
      break;

    case actions.USER_PROFILE_FETCH_REQUEST_SUCCESS: {
      const userData = CommonUtils.getObjectValueIfEmpty(action, 'payload.data.data', {});
      changes = {
        isFetchingUserProfile: false,
        isFetchedUserProfile: true,
        userData,
      };
    }
      break;

    case actions.USER_PROFILE_FETCH_REQUEST_FAILED:
      changes = {
        isFetchingUserProfile: false,
        isFetchedUserProfile: false,
        userData: null,
      };
      break;

    case actions.LOGOUT_REQUEST:
      AuthenticationHelper.clearAuthToken();
      changes = {
        isAuthenticated: false,
      };
      break;

    default:
      changes = {};
      break;
  }

  return CommonUtils.clone(state, changes);
};
