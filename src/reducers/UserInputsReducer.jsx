import { actions } from './../constants/Constants';
import CommonUtils from './../utils/CommonUtils';
import CommonHelper from './../helpers/CommonHelper';

const initialState = {
  selectedHotel: null,
};

export default (state = initialState, action = {}) => {
  let changes = {};

  switch (action.type) {

    case actions.LOGIN_RESET_STORE:
      CommonHelper.clearStorageSelectedHotel();
      changes = { ...initialState };
      break;

    case actions.LOGOUT_REQUEST:
      CommonHelper.clearStorageSelectedHotel();
      changes = { ...initialState };
      break;

    case actions.OWNER_CHANGED_SELECTED_HOTEL: {
      const selectedHotel = CommonUtils.getObjectValueIfEmpty(action, 'payload.hotel', null);
      const selectedHotelId = CommonUtils.getObjectValueIfEmpty(selectedHotel, 'propertyId', 0);
      // save selection to storage
      CommonHelper.saveSelectedHotelToStorage(selectedHotelId);

      changes = { selectedHotel };
    }
      break;

    default:
      changes = {};
      break;
  }

  return CommonUtils.clone(state, changes);
};
