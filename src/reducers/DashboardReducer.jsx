import { actions } from './../constants/Constants';
import CommonUtils from './../utils/CommonUtils';

const initialState = {
  isMenuListLoading: false,
  hotelSummary: null,
};

export default (state = initialState, action = {}) => {
  let changes = {};

  switch (action.type) {
    case actions.DASHBOARD_ITEMS_STORE_RESET:
      changes = { ...initialState };
      break;

    case actions.LOGOUT_REQUEST:
      changes = { ...initialState };
      break;

    case actions.LOGIN_RESET_STORE:
      changes = { ...initialState };
      break;

    case actions.OWNER_CHANGED_SELECTED_HOTEL:
      changes = { ...initialState };
      break;

    case actions.DASHBOARD_ITEMS_LOADING:
      changes = {
        isMenuListLoading: true,
        hotelSummary: null,
      };
      break;

    case actions.DASHBOARD_ITEMS_LOADED:
      changes = {
        isMenuListLoading: false,
        hotelSummary: CommonUtils.getObjectValueIfEmpty(action, 'payload.hotelSummary', []),
      };
      break;

    case actions.DASHBOARD_ITEMS_LOADING_FAILED:
      changes = {
        isMenuListLoading: false,
        hotelSummary: null,
      };
      break;

    default:
      changes = {};
      break;
  }

  return CommonUtils.clone(state, changes);
};
