import { actions } from './../constants/Constants';
import CommonUtils from './../utils/CommonUtils';

const initialState = {
  isSummaryLoading: false,
  isReviewsLoading: false,
  isReviewsAllPagesLoaded: false,
  customerRatingsSummary: null,
};

export default (state = initialState, action = {}) => {
  let changes = {};

  switch (action.type) {
    case actions.CUSTOMER_RATINGS_STORE_RESET:
      changes = { ...initialState };
      break;

    case actions.LOGOUT_REQUEST:
      changes = { ...initialState };
      break;

    case actions.LOGIN_RESET_STORE:
      changes = { ...initialState };
      break;

    case actions.OWNER_CHANGED_SELECTED_HOTEL:
      changes = { ...initialState };
      break;

    case actions.CUSTOMER_RATINGS_LOADING:
      changes = {
        isSummaryLoading: true,
        isReviewsLoading: false,
        isReviewsAllPagesLoaded: false,
        customerRatingsSummary: null,
      };
      break;

    case actions.CUSTOMER_RATINGS_LOADED:
      changes = {
        isSummaryLoading: false,
        customerRatingsSummary: CommonUtils.getObjectValueIfEmpty(action, 'payload.customerRatingsSummary', []),
      };
      break;

    case actions.CUSTOMER_RATINGS_LOADING_FAILED:
      changes = {
        isSummaryLoading: false,
        customerRatingsSummary: null,
      };
      break;

    case actions.CUSTOMER_REVIEWS_LOADING:
      changes = {
        isReviewsLoading: true,
      };
      break;

    case actions.CUSTOMER_REVIEWS_LOADED: {
      const existingReviews = CommonUtils.getObjectValueIfEmpty(state.customerRatingsSummary, 'pageReviews.content', []);
      const newReviews = CommonUtils.getObjectValueIfEmpty(action, 'payload.customerReviews.content', []);
      const cloneSummary = { ...state.customerRatingsSummary };
      cloneSummary.pageReviews.content = [...existingReviews, ...newReviews];

      changes = {
        isReviewsLoading: false,
        customerRatingsSummary: cloneSummary,
      };
    }
      break;

    case actions.CUSTOMER_REVIEWS_LOADING_FAILED:
      changes = {
        isReviewsLoading: false,
        isReviewsAllPagesLoaded: true,
      };
      break;

    default:
      changes = {};
      break;
  }

  return CommonUtils.clone(state, changes);
};
