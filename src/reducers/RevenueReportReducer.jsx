import { actions } from './../constants/Constants';
import CommonUtils from './../utils/CommonUtils';

const initialState = {
  isSummaryLoading: false,
  revenueReportSummary: null,
};

export default (state = initialState, action = {}) => {
  let changes = {};

  switch (action.type) {
    case actions.REVENUE_REPORT_STORE_RESET:
      changes = { ...initialState };
      break;

    case actions.LOGOUT_REQUEST:
      changes = { ...initialState };
      break;

    case actions.LOGIN_RESET_STORE:
      changes = { ...initialState };
      break;

    case actions.OWNER_CHANGED_SELECTED_HOTEL:
      changes = { ...initialState };
      break;

    case actions.REVENUE_REPORT_LOADING:
      changes = {
        isSummaryLoading: true,
        revenueReportSummary: null,
      };
      break;

    case actions.REVENUE_REPORT_LOADED:
      changes = {
        isSummaryLoading: false,
        revenueReportSummary: CommonUtils.getObjectValueIfEmpty(action, 'payload.revenueReportSummary', {}),
      };
      break;

    case actions.REVENUE_REPORT_LOADING_FAILED:
      changes = {
        isSummaryLoading: false,
        revenueReportSummary: null,
      };
      break;

    default:
      changes = {};
      break;
  }

  return CommonUtils.clone(state, changes);
};
