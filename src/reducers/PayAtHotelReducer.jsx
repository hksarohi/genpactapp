import { actions } from './../constants/Constants';
import CommonUtils from './../utils/CommonUtils';

const initialState = {
  isSummaryLoading: false,
  reportSummary: null,
};

export default (state = initialState, action = {}) => {
  let changes = {};

  switch (action.type) {
    case actions.PAY_AT_HOTEL_SUMMARY_STORE_RESET:
      changes = { ...initialState };
      break;

    case actions.LOGOUT_REQUEST:
      changes = { ...initialState };
      break;

    case actions.LOGIN_RESET_STORE:
      changes = { ...initialState };
      break;

    case actions.OWNER_CHANGED_SELECTED_HOTEL:
      changes = { ...initialState };
      break;

    case actions.PAY_AT_HOTEL_LOADING:
      changes = {
        isSummaryLoading: true,
        reportSummary: null,
      };
      break;

    case actions.PAY_AT_HOTEL_LOADED:
      changes = {
        isSummaryLoading: false,
        reportSummary: CommonUtils.getObjectValueIfEmpty(action, 'payload.payAtHotelSummary', []),
      };
      break;

    case actions.PAY_AT_HOTEL_LOADING_FAILED:
      changes = {
        isSummaryLoading: false,
        reportSummary: null,
      };
      break;

    default:
      changes = {};
      break;
  }

  return CommonUtils.clone(state, changes);
};
