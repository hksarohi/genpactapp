import React from 'react';
import PropTypes from 'prop-types';

import CommonInfoTabsRoot from './../common/CommonInfoTabsRoot';
import OutOfOrderReportItemDetails from './OutOfOrderReportItemDetails';
import NoUnderMaintenanceRooms from './NoUnderMaintenanceRooms';

import CommonUtils from './../../utils/CommonUtils';
import DateUtils from './../../utils/DateUtils';
import Formatter from './../../helpers/Formatter';
import Loader from './../common/Loader';

class UnderMaintenanceRoomsReport extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      reportDetailsDate: null,
    };

    this.onReportItemClick = this.onReportItemClick.bind(this);
    this.closeReportDetailPage = this.closeReportDetailPage.bind(this);
  }

  onReportItemClick(reportDetailsDate) {
    this.setState({ reportDetailsDate });
  }

  getReportDetailPageItem() {
    const { reportDetailsDate } = this.state;
    if (CommonUtils.isEmpty(reportDetailsDate)) return null;

    const reportDetailItem = CommonUtils.getObjectValue(this.props.items, reportDetailsDate);
    if (CommonUtils.isEmpty(reportDetailItem)) return null;

    reportDetailItem.reportDate = this.state.reportDetailsDate;
    return reportDetailItem;
  }

  getReportItems() {
    const reportItems = [];

    let counter = 0;
    Object.keys(this.props.items).forEach((reportDate) => {
      if (CommonUtils.keyExists(reportDate, this.props.items)) {
        const row = CommonUtils.getObjectValue(this.props.items, reportDate, {});
        const date = DateUtils.getDateInFormat(reportDate, DateUtils.dMY, ' ');
        const roomCount = Formatter.formatCount(CommonUtils.getObjectValueIfEmpty(row, 'rooms', '--'));
        counter += 1;

        const tableRow = (
          <li key={counter} onClick={() => { this.onReportItemClick(reportDate); }} className="clearfix">
            <span className="info-date"> {date} </span>
            <strong className="info-room">
              <span className="info-room-count">{roomCount} </span>
              <span className="info-room-room"> {roomCount > 1 ? 'Rooms' : 'Room'} </span>
            </strong>
          </li>
        );

        reportItems.push(tableRow);
      }
    });

    return reportItems;
  }

  closeReportDetailPage() {
    this.setState({ reportDetailsDate: null });
  }

  render() {
    const reportItems = this.getReportItems();

    let reportData = null;
    if (this.props.isRoomsLoading) {
      reportData = (<Loader type={2} extraClassName="loader-blue" />);
    } else if (CommonUtils.isEmpty(reportItems)) {
      reportData = (<NoUnderMaintenanceRooms />);
    } else {
      reportData = (
        <div className="info-rows-data">
          <ul>
            {reportItems}
          </ul>
        </div>
      );
    }

    let reportDetailPage = null;
    const reportDetailPageItem = this.getReportDetailPageItem();
    if (reportDetailPageItem !== null) {
      reportDetailPage = (
        <OutOfOrderReportItemDetails
          item={reportDetailPageItem}
          closeDetailsPageFn={this.closeReportDetailPage}
        />
      );
    }

    return (
      <div className="infotabs-container">
        <CommonInfoTabsRoot
          callDefaultSelectionFn
          selectedTab={this.props.selectedTab}
          onTabClickFn={this.props.onTabClickFn}
        />

        <div className="info-rows-container">
          <div className="info-rows-heading">
            <h4> {this.props.reportHeading}</h4>
          </div>
          {reportData}
        </div>
        {reportDetailPage}

      </div>
    );
  }
}

UnderMaintenanceRoomsReport.defaultProps = {
  isRoomsLoading: false,
  items: {},
  reportHeading: '',
  selectedTab: 1,
  onTabClickFn() {},
};

UnderMaintenanceRoomsReport.propTypes = {
  isRoomsLoading: PropTypes.bool,
  items: PropTypes.object,
  reportHeading: PropTypes.string,
  selectedTab: PropTypes.number,
  onTabClickFn: PropTypes.func,
};

export default UnderMaintenanceRoomsReport;
