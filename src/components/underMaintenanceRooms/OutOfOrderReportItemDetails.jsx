import React from 'react';
import PropTypes from 'prop-types';
import CommonUtils from './../../utils/CommonUtils';
import DateUtils from './../../utils/DateUtils';
import Formatter from './../../helpers/Formatter';
import Modal from './../common/Modal';

class OutOfOrderReportItemDetails extends React.Component {

  constructor(props) {
    super(props);

    this.onCloseClickHandler = this.onCloseClickHandler.bind(this);
  }

  onCloseClickHandler() {
    this.props.closeDetailsPageFn();
  }

  getTableRowItems() {
    const roomDetails = CommonUtils.getObjectValueIfEmpty(this.props.item, 'roomDetails', []);

    let counter = 0;
    const tableItems = roomDetails.map((item) => {
      const itemValueFirst = CommonUtils.getObjectValueIfEmpty(item, 'roomNumber', '');
      const itemValueSecond = CommonUtils.getObjectValueIfEmpty(item, 'reason', '');
      counter += 1;

      return (
        <li key={counter} className="clearfix">
          <span className="room-no">{itemValueFirst}</span>
          <span className="room-disc">{itemValueSecond}</span>
        </li>
      );
    });

    return tableItems;
  }

  render() {
    const tableItems = this.getTableRowItems();
    const reportDate = DateUtils.getDateInFormat(CommonUtils.getObjectValue(this.props.item, 'reportDate'), DateUtils.dMY, ' ');
    const roomsCount = Formatter.formatCount(CommonUtils.getObjectValue(this.props.item, 'rooms', '--'));

    return (
      <Modal closeFn={this.onCloseClickHandler}>
        <div className="info-rows-popup">
          <div className="info-rows-header">
            <h4>
              <span className="date"> {reportDate} </span>
              <span className="count"> {roomsCount} </span>
              <span className="rooms">Room(s)</span>
            </h4>
          </div>
          <div className="info-rows-content">
            <div className="info-rows-content-header clearfix">
              <span className="info-rows-content-header-first">
              Room No.
            </span>
              <span className="info-rows-content-header-second">
              Reason
            </span>
            </div>
            <div className="info-rows-content-section">
              <ul className="clearfix">
                {tableItems}
              </ul>
            </div>
          </div>
          <div
            role="button"
            onClick={this.props.closeDetailsPageFn}
            className="info-rows-footer"
          >
          Done
        </div>
        </div>
      </Modal>
    );
  }
}

OutOfOrderReportItemDetails.defaultProps = {
  item: {},
  closeDetailsPageFn() {},
};

OutOfOrderReportItemDetails.propTypes = {
  item: PropTypes.object,
  closeDetailsPageFn: PropTypes.func,
};

export default OutOfOrderReportItemDetails;
