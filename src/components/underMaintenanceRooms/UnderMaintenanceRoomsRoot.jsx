import React from 'react';
import PropTypes from 'prop-types';

import UnderMaintenanceRoomsCard from './UnderMaintenanceRoomsCard';
import UnderMaintenanceRoomsReport from './UnderMaintenanceRoomsReport';

import CommonUtils from './../../utils/CommonUtils';
import DateUtils from './../../utils/DateUtils';

class UnderMaintenanceRoomsRoot extends React.Component {
  render() {
    const tabsInfoItems = CommonUtils.getObjectValueIfEmpty(this.props.tabsInfo, 'items', {});
    const tabsInfoSelectedTab = CommonUtils.getObjectValueIfEmpty(this.props.tabsInfo, 'selectedTab', 1);
    const tabsInfoOnTabClickFn = CommonUtils.getObjectValueIfEmpty(this.props.tabsInfo, 'onTabClickFn', () => {});
    let reportHeading = null;

    if (!CommonUtils.isEmpty(this.props.reportStartDate) && !CommonUtils.isEmpty(this.props.reportEndDate)) {
      reportHeading = `${DateUtils.getDateInFormat(this.props.reportStartDate, DateUtils.dMY, ' ')} - ${
        DateUtils.getDateInFormat(this.props.reportEndDate, DateUtils.dMY, ' ')}`;
    }

    return (
      <div className="main-container">
        <UnderMaintenanceRoomsCard
          title={this.props.roomsInfo.heading}
          items={this.props.roomsInfo.items}
        />

        <UnderMaintenanceRoomsReport
          isRoomsLoading={this.props.isRoomsLoading}
          items={tabsInfoItems}
          selectedTab={tabsInfoSelectedTab}
          onTabClickFn={tabsInfoOnTabClickFn}
          reportHeading={reportHeading}
        />
      </div>
    );
  }
}

UnderMaintenanceRoomsRoot.defaultProps = {
  isRoomsLoading: false,
  roomsInfo: {},
  tabsInfo: {},
  reportStartDate: '',
  reportEndDate: '',
};

UnderMaintenanceRoomsRoot.propTypes = {
  isRoomsLoading: PropTypes.bool,
  roomsInfo: PropTypes.shape({
    heading: PropTypes.string,
    items: PropTypes.arrayOf(PropTypes.object),
  }),
  tabsInfo: PropTypes.shape({
    items: PropTypes.object,
    selectedTab: PropTypes.number,
    onTabClickFn: PropTypes.func.isRequired,
  }),
  reportStartDate: PropTypes.string,
  reportEndDate: PropTypes.string,
};

export default UnderMaintenanceRoomsRoot;
