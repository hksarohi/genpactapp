import React from 'react';

class NoUnderMaintenanceRooms extends React.Component {
  render() {
    return (
      <div className="no-undermaintenance-container">
        <i className="no-undermaintenance-icon" />
        <h2> Woah! You don't seem to have any open issues</h2>
      </div>
    );
  }
}

NoUnderMaintenanceRooms.defaultProps = {

};

NoUnderMaintenanceRooms.propTypes = {

};


export default NoUnderMaintenanceRooms;
