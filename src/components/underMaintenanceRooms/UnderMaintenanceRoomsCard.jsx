import React from 'react';
import PropTypes from 'prop-types';

import InfoBoxes from '../common/InfoBoxes';

class UnderMaintenanceRoomsCard extends React.Component {
  render() {
    const title = this.props.title;
    const items = this.props.items;

    return (
      <div className="infoboxes-container">
        <h3> {title}</h3>
        <InfoBoxes
          items={items}
        />
      </div>
    );
  }
}

UnderMaintenanceRoomsCard.defaultProps = {
  title: '',
  items: [],
};

UnderMaintenanceRoomsCard.propTypes = {
  title: PropTypes.string.isRequired,
  items: PropTypes.arrayOf(PropTypes.object).isRequired,
};

export default UnderMaintenanceRoomsCard;
