import React from 'react';

class HelpRoot extends React.Component {
  render() {
    return (
      <div className="help-container">
        <h3> Contact FabHotels </h3>
        <p>
          We would love to hear from you, and answer any question you may have on anything related to a FabHotels.
           You can call us on <a href="tel:07042424242">070-4242-4242</a>  (24x7) if you would want to talk to us, or alternately, write to us:
        </p>
        <p> For bookings: <a href="mailto:bookings@fabhotels.com">bookings@fabhotels.com</a> <br />
        For stay feedback: <a href="mailto:ceo@fabhotels.com">ceo@fabhotels.com</a> <br />
        For making your property a FabHotel: <a href="mailto:partners@fabhotels.com">partners@fabhotels.com</a> <br />
        For career : <a href="mailto:jobs@fabhotels.com">jobs@fabhotels.com</a> <br />
        Correspondence Address : Level 6, Regus, JMD Regent Square, MG Road, Gurgaon, Haryana - 122002..<br />
        We would also love to stay connected on <a href="https://www.facebook.com/fabhotelscom">Facebook</a>  and <a href="https://twitter.com/fabhotels">Twitter.</a>
        </p>
      </div>
    );
  }
}

export default HelpRoot;
