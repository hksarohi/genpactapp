import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import DashboardItem from './DashboardItem';
import CommonUtils from './../../utils/CommonUtils';

class DashboardMenuCard extends React.Component {
  getMenuItems() {
    let counter = 0;
    const menuItems = this.props.menuItemList.map((item) => {
      const isItemEnabled = CommonUtils.getObjectValueIfEmpty(item, 'isItemEnabled', false);

      // Display enabled items only 
      if (!isItemEnabled) return null;

      counter += 1;

      const ratingCount = CommonUtils.getObjectValueIfEmpty(item, 'ratingCount', null);
      const showRating = CommonUtils.getObjectValueIfEmpty(item, 'showRating', (ratingCount !== null));
      const itemHeading = CommonUtils.getObjectValueIfEmpty(item, 'itemHeading', null);
      const showItemHeading = CommonUtils.getObjectValueIfEmpty(item, 'showItemHeading', (itemHeading !== null));
      const navigateTo = CommonUtils.getObjectValueIfEmpty(item, 'navigateTo', '#');
      const itemRatingClass = CommonUtils.getObjectValueIfEmpty(item, 'itemRatingClass', null);
      const itemHeadingClass = CommonUtils.getObjectValueIfEmpty(item, 'itemHeadingClass', null);

      return (
        <li key={counter}>
          <DashboardItem
            showRating={showRating}
            showItemHeading={showItemHeading}
            ratingCount={ratingCount}
            itemHeading={itemHeading}
            navigateTo={navigateTo}
            itemRatingClass={itemRatingClass}
            itemHeadingClass={itemHeadingClass}
          />
        </li>
      );
    });

    return menuItems;
  }

  render() {
    const cardHeading = this.props.showMenuHeading ? (<h3 className={this.props.menuCardHeadingIcon}> {this.props.menuCardHeading} </h3>) : '';
    const menuItems = this.getMenuItems();

    return (
      <div className="dashboard-menu-card">
        {cardHeading}
        <ul className="clearfix">
          {menuItems}
        </ul>
      </div>

    );
  }
}

// Specifies the default values for props:
DashboardMenuCard.defaultProps = {
  showMenuHeading: true,
  menuCardHeading: null,
  menuCardHeadingIcon: null,
  menuItemList: [],
};

// prop types validation
DashboardMenuCard.propTypes = {
  showMenuHeading: PropTypes.bool,
  menuCardHeading: PropTypes.string,
  menuCardHeadingIcon: PropTypes.string,
  menuItemList: PropTypes.arrayOf(PropTypes.object),
};

export default connect(null)(DashboardMenuCard);
