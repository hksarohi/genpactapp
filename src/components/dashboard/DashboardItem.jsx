import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

class DashboardItem extends React.Component {

  render() {
    const itemHeadingClass = `${this.props.itemHeadingDefaultClass} ${this.props.itemHeadingClass}`;
    const itemRatingClass = `${this.props.itemRatingDefaultClass} ${this.props.itemRatingClass}`;

    const itemRating = this.props.showRating ? (<span className={itemRatingClass}>{this.props.ratingCount}</span>) : '';
    const itemHeading = this.props.showItemHeading ? (<span className={itemHeadingClass}>{this.props.itemHeading}</span>) : '';

    return (
      <Link to={this.props.navigateTo} className="animated fadeInLeft">
        {itemRating}
        {itemHeading}
      </Link>
    );
  }
}

// Specifies the default values for props:
DashboardItem.defaultProps = {
  showRating: true,
  showItemHeading: true,
  ratingCount: null,
  itemHeading: null,
  navigateTo: '#',
  itemHeadingDefaultClass: 'customer-text',
  itemRatingDefaultClass: 'customer-rating',
  itemRatingClass: null,
  itemHeadingClass: null,
};

// prop types validation
DashboardItem.propTypes = {
  showRating: PropTypes.bool,
  showItemHeading: PropTypes.bool,
  ratingCount: PropTypes.string,
  itemHeading: PropTypes.string,
  navigateTo: PropTypes.string,
  itemHeadingDefaultClass: PropTypes.string,
  itemRatingDefaultClass: PropTypes.string,
  itemRatingClass: PropTypes.string,
  itemHeadingClass: PropTypes.string,
};

export default connect(null)(DashboardItem);
