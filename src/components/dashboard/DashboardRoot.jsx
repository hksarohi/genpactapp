import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import DashboardMenuCard from './DashboardMenuCard';
import CommonUtils from './../../utils/CommonUtils';

class DashboardRoot extends React.Component {
  getMenuCards() {
    let counter = 0;

    const menuCards = this.props.menuCardList.map((item) => {
      counter += 1;

      const menuCardHeading = CommonUtils.getObjectValueIfEmpty(item, 'menuCardHeading', null);
      const menuCardHeadingIcon = CommonUtils.getObjectValueIfEmpty(item, 'menuCardHeadingIcon', null);
      const showMenuHeading = CommonUtils.getObjectValueIfEmpty(item, 'showMenuHeading', (menuCardHeading !== null));
      const menuItemList = CommonUtils.getObjectValueIfEmpty(item, 'menuItemList', []);

      return (
        <DashboardMenuCard
          key={counter}
          showMenuHeading={showMenuHeading}
          menuCardHeading={menuCardHeading}
          menuCardHeadingIcon={menuCardHeadingIcon}
          menuItemList={menuItemList}
        />
      );
    });

    return menuCards;
  }

  render() {
    const menuCards = this.getMenuCards();

    return (
      <div className="dashboard">
        {menuCards}
      </div>
    );
  }
}

// Specifies the default values for props:
DashboardRoot.defaultProps = {
  menuCardList: [],
};

// prop types validation
DashboardRoot.propTypes = {
  menuCardList: PropTypes.arrayOf(PropTypes.object),
};

export default connect(null)(DashboardRoot);
