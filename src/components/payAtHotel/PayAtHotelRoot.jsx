import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import CommonInfoTabsRoot from './../common/CommonInfoTabsRoot';
import InfoTable from '../common/InfoTable';

class PayAtHotelRoot extends React.Component {
  render() {
    return (
      <div className="main-container">
        <div className="infotabs-container">
          <CommonInfoTabsRoot
            callDefaultSelectionFn
            showThirdTab={false}
            selectedTab={this.props.selectedTab}
            onTabClickFn={this.props.onTabClickFn}
          />

          <div className="pay-at-hotel-table">
            <InfoTable
              isSummaryLoading={this.props.isSummaryLoading}
              headings={this.props.headings}
              itemKeys={this.props.itemKeys}
              items={this.props.items}
              formatRowValue={this.props.formatRowValue}
            />
          </div>
        </div>
      </div>
    );
  }
}

PayAtHotelRoot.defaultProps = {
  isSummaryLoading: false,
  headings: [],
  itemKeys: [],
  items: [],
  selectedTab: 1,
  onTabClickFn() {},
  formatRowValue() {},
};

PayAtHotelRoot.propTypes = {
  isSummaryLoading: PropTypes.bool,
  headings: PropTypes.arrayOf(PropTypes.string),
  itemKeys: PropTypes.arrayOf(PropTypes.string),
  items: PropTypes.arrayOf(PropTypes.object),
  selectedTab: PropTypes.number,
  onTabClickFn: PropTypes.func,
  formatRowValue: PropTypes.func,
};

export default connect(null)(PayAtHotelRoot);
