import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

class SideBarUserProfile extends React.Component {

  render() {
    return (
      <div className="profile-wrapper clearfix">
        <div className="profile-pic-wrapper"><span>{this.props.userProfileChar}</span></div>
        <div className="profile-title">
          <span>Welcome, </span>
          <strong>{this.props.userFullName}</strong>
        </div>
      </div>
    );
  }
}

// Specifies the default values for props:
SideBarUserProfile.defaultProps = {
  userFullName: null,
  userProfileChar: null,
};

// prop types validation
SideBarUserProfile.propTypes = {
  userFullName: PropTypes.string.isRequired,
  userProfileChar: PropTypes.string.isRequired,
};

export default connect(null)(SideBarUserProfile);
