import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import SideBarUserProfile from './SideBarUserProfile';
import SideBarHotelItem from './SideBarHotelItem';
import CommonUtils from './../../utils/CommonUtils';

class SideBarHead extends React.Component {
  getUserNameFirstChar(userName) {
    if (CommonUtils.isEmpty(userName) || typeof userName !== 'string') return '';

    const name = userName.trim();
    return !CommonUtils.isEmpty(name[0]) ? name[0].toUpperCase() : '';
  }

  getUserFullName() {
    const fullName = `${CommonUtils.getObjectValueIfEmpty(this.props.userProfile, 'firstName', '')} ${
      CommonUtils.getObjectValueIfEmpty(this.props.userProfile, 'lastName', '')}`;

    return fullName;
  }

  render() {
    const { selectedHotel, hotelList, showingHotelList } = this.props;
    const userFullName = this.getUserFullName();
    let hotelItemContainerClass = '';

    if (hotelList.length > 1) {
      hotelItemContainerClass = showingHotelList ? 'hotel-item-up' : 'hotel-item-down';
    }

    const hotelItem = CommonUtils.isEmpty(selectedHotel) ? null :
      (<SideBarHotelItem
        containerClass={hotelItemContainerClass}
        hotelDetail={selectedHotel}
        onItemClick={this.props.onSelectedHotelClick}
      />);

    return (
      <div className="sidebar-header">
        <SideBarUserProfile
          userFullName={userFullName}
          userProfileChar={this.getUserNameFirstChar(userFullName)}
        />
        {hotelItem}
      </div>
    );
  }
}

// Specifies the default values for props:
SideBarHead.defaultProps = {
  userProfile: {},
  selectedHotel: {},
  hotelList: [],
  showingHotelList: false,
  onSelectedHotelClick() {},
};

// prop types validation
SideBarHead.propTypes = {
  userProfile: PropTypes.object,
  selectedHotel: PropTypes.object,
  hotelList: PropTypes.array,
  showingHotelList: PropTypes.bool,
  onSelectedHotelClick: PropTypes.func,
};

export default connect(null)(SideBarHead);
