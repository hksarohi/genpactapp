import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { assetUrl } from './../../constants/Constants';

class SideBarHotelItem extends React.Component {
  constructor(props) {
    super(props);

    this.onHotelItemClick = this.onHotelItemClick.bind(this);
  }

  onHotelItemClick() {
    this.props.onItemClick(this.props.hotelDetail);
  }

  render() {
    const { hotelDetail } = this.props;

    return (
      <div
        role="button"
        className={`hotel-list ${this.props.containerClass}`}
        onClick={this.onHotelItemClick}
      >
        <div className="clearfix">
          <div className="hotel-pic">
            <img alt={hotelDetail.propertyName} src={assetUrl.IMAGE_BASE_URL + hotelDetail.propertyUrl} />
          </div>
          <div className="hotel-title"> {hotelDetail.propertyName} </div>
        </div>
      </div>
    );
  }
}

// Specifies the default values for props:
SideBarHotelItem.defaultProps = {
  hotelDetail: {},
  onItemClick() {},
  containerClass: '',
};

// prop types validation
SideBarHotelItem.propTypes = {
  hotelDetail: PropTypes.shape({
    id: PropTypes.number,
    propertyName: PropTypes.string.isRequired,
    propertyUrl: PropTypes.string.isRequired,
  }),
  onItemClick: PropTypes.func,
  containerClass: PropTypes.string,
};

export default connect(null)(SideBarHotelItem);
