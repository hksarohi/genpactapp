import React from 'react';
import Swipeable from 'react-swipeable';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import SideBarHead from './SideBarHead';
import CommonUtils from './../../utils/CommonUtils';
import CommonHelper from './../../helpers/CommonHelper';
import SideBarHotelItem from './SideBarHotelItem';
import Loader from './../common/Loader';
import Logout from './../logout/Logout';

import { fetchOwnerHotelsIfNeeded, selectOwnerHotel } from './../../actions/UserMetaAction';
import { fetchUserProfileIfNeeded, logoutUser } from './../../actions/LoginProcessAction';

class SideBar extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      showHotelList: false,
      isLoggingOut: false,
    };

    // Bind functions
    this.onSelectedHotelClick = this.onSelectedHotelClick.bind(this);
    this.onHotelListItemClick = this.onHotelListItemClick.bind(this);
    this.onLogoutClick = this.onLogoutClick.bind(this);
    this.onLogoutConfirm = this.onLogoutConfirm.bind(this);
    this.onLogoutCancel = this.onLogoutCancel.bind(this);
  }

  componentDidMount() {
    this.props.fetchOwnerHotelsIfNeeded();
    this.props.fetchUserProfileIfNeeded();
  }

  componentDidUpdate() {
    if (this.props.openSideBar) {
      CommonHelper.disableBodyScroll();
    } else {
      CommonHelper.enableBodyScroll();
    }
  }

  onSelectedHotelClick() {
    const hotelList = CommonUtils.getObjectValueIfEmpty(this.props.userMeta, 'hotels', []);
    // do not show hotel list if one or less hotel
    if (hotelList.length <= 1) return;

    this.setState({
      showHotelList: !this.state.showHotelList,
    });
  }

  onHotelListItemClick(hotelDetail) {
    const oldSelectedHotel = CommonUtils.getObjectValue(this.props.selectedHotel, 'propertyId');
    const newSelectedHotel = CommonUtils.getObjectValue(hotelDetail, 'propertyId');

    if (oldSelectedHotel !== newSelectedHotel) this.props.selectOwnerHotel(hotelDetail);

    this.setState({ showHotelList: false });
    this.props.closeSideBarFn();
  }

  onLogoutClick() {
    this.setState({ isLoggingOut: true });
  }

  onLogoutConfirm() {
    this.props.logoutUser();
  }

  onLogoutCancel() {
    this.setState({ isLoggingOut: false });
  }

  getUserHotelListElements() {
    const hotelList = CommonUtils.getObjectValueIfEmpty(this.props.userMeta, 'hotels', []);
    // do not show hotel list if one or less hotel
    if (hotelList.length < 1) return null;

    let counter = 0;
    const hotelElements = hotelList.map((item) => {
      counter += 1;
      return (
        <SideBarHotelItem
          key={counter}
          hotelDetail={item}
          onItemClick={this.onHotelListItemClick}
        />
      );
    });
    return hotelElements;
  }

  render() {
    let sideBarClassName = 'sidebar';
    let backDropClassName = 'back-drop';
    let overlay = 'overlay';
    let logout = null;

    if (!this.props.openSideBar || this.state.isLoggingOut) {
      sideBarClassName += ' sidebar-hidden';
      backDropClassName += ' back-drop-hidden';
      overlay += ' overlay-hidden';
    }

    if (this.state.isLoggingOut) {
      logout = (
        <Logout
          onLogout={this.onLogoutConfirm}
          onCloseFn={this.onLogoutCancel}
        />
      );
    }

    let hotelListClass = 'hotel-list-container  -hotel-list-hidden';
    const hotelListElements = this.getUserHotelListElements();
    const loader = this.props.userMeta.isHotelsLoading ? (<Loader type={1} />) : null;

    if (this.state.showHotelList) hotelListClass = 'hotel-list-container';
    const hotelList = CommonUtils.getObjectValueIfEmpty(this.props.userMeta, 'hotels', []);


    return (
      <Swipeable onSwipedRight={this.props.closeSideBarFn} >
        <div role="button" tabIndex="-1" onClick={this.props.closeSideBarFn} className={backDropClassName} />
        <div className={sideBarClassName}>

          <SideBarHead
            userProfile={this.props.userProfile}
            selectedHotel={this.props.selectedHotel}
            showingHotelList={this.state.showHotelList}
            hotelList={hotelList}
            onSelectedHotelClick={this.onSelectedHotelClick}
          />

          <div className="sidebar-content">
            <div className={hotelListClass}>
              {hotelListElements}
            </div>
            <ul className="sidebar-link clearfix">
              <li>
                <Link to="/dashboard" onClick={this.props.closeSideBarFn} className="active"> Dashboard</Link>
              </li>
              <li>
                <Link to="/help" onClick={this.props.closeSideBarFn}> Help </Link>
              </li>
            </ul>
          </div>
          <div role="button" className="logout" onClick={this.onLogoutClick}>Logout</div>
          {logout}
        </div>
        <div className={overlay} />
        {loader}
      </Swipeable>
    );
  }
}

// Specifies the default values for props:
SideBar.defaultProps = {
  userProfile: {},
  userMeta: {},
  selectedHotel: {},
  fetchOwnerHotelsIfNeeded() {},
  fetchUserProfileIfNeeded() {},
  logoutUser() {},
  selectOwnerHotel() {},
  openSideBar: false,
  closeSideBarFn() {},
};

// prop types validation
SideBar.propTypes = {
  userProfile: PropTypes.object,
  userMeta: PropTypes.object,
  selectedHotel: PropTypes.object,
  fetchOwnerHotelsIfNeeded: PropTypes.func,
  selectOwnerHotel: PropTypes.func,
  fetchUserProfileIfNeeded: PropTypes.func,
  logoutUser: PropTypes.func,
  openSideBar: PropTypes.bool,
  closeSideBarFn: PropTypes.func,
};

function mapStateToProps(state) {
  const { userMeta, loginUser, userInputs } = state;

  return {
    userMeta,
    userProfile: loginUser.userData,
    selectedHotel: userInputs.selectedHotel,
  };
}

export default connect(mapStateToProps, {
  fetchOwnerHotelsIfNeeded,
  selectOwnerHotel,
  fetchUserProfileIfNeeded,
  logoutUser,
})(SideBar);
