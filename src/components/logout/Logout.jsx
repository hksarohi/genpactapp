import React from 'react';
import PropTypes from 'prop-types';

import Modal from './../common/Modal';
import ConfirmBox from './../common/ConfirmBox';

class Logout extends React.Component {

  render() {
    return (
      <Modal closeFn={this.props.onCloseFn}>
        <ConfirmBox
          title="Are you sure you want to Logout?"
          cancelBtnText="CANCEL"
          okBtnText="LOGOUT"
          onOkClick={this.props.onLogout}
          onCancelClick={this.props.onCloseFn}
        />
      </Modal>
    );
  }
}

// Specifies the default values for props:
Logout.defaultProps = {
  onCloseFn() {},
  onLogout() {},
};

// prop types validation
Logout.propTypes = {
  onCloseFn: PropTypes.func,
  onLogout: PropTypes.func,
};

export default Logout;
