import React from 'react';
import PropTypes from 'prop-types';

import ScatterGraph from './../common/ScatterGraph';
import CommonInfoTabsRoot from './../common/CommonInfoTabsRoot';
import NoDataFound from './../common/NoDataFound';

import CommonUtils from './../../utils/CommonUtils';
import DateUtils from './../../utils/DateUtils';
import Formatter from './../../helpers/Formatter';
import Loader from './../common/Loader';

class RevenueReport extends React.Component {
  getReportItems() {
    const graphValues = [];

    const items = CommonUtils.getObjectValueIfEmpty(this.props.items, 'dayWiseData', []);
    Object.keys(items).forEach((reportDate) => {
      if (CommonUtils.keyExists(reportDate, items)) {
        const date = DateUtils.getInstance(reportDate);
        const amount = Formatter.formatAmount(CommonUtils.getObjectValue(items, reportDate, 0));

        const reportData = {
          x: date,
          y: amount,
        };

        graphValues.push(reportData);
      }
    });

    return graphValues;
  }

  render() {
    const reportAverage = CommonUtils.getObjectValueIfEmpty(this.props.items, 'average', '--');
    const averageData = (<div>Average Daily Rate (ADR): &#x20B9; {Formatter.priceWithCommas(reportAverage)}</div>);

    const reportItems = this.getReportItems();

    let graphData = (<NoDataFound />);

    if (this.props.isSummaryLoading) {
      graphData = (<Loader type={2} extraClassName="loader-blue" />);
    } else if (!CommonUtils.isEmpty(reportItems)) {
      const itemValues = Object.values(this.props.items.dayWiseData);
      const minValue = Math.floor((Math.min(...itemValues)) / 100) * 100;
      const maxValue = Math.ceil((Math.max(...itemValues)) / 100) * 100;

      graphData = (
        <div>
          <ScatterGraph
            data={reportItems}
            yTicksMinVal={minValue}
            yTicksMaxVal={maxValue}
          />
          <div className="graph-average">
            {averageData}
          </div>
        </div>
      );
    }

    return (
      <div className="infotabs-container">
        <CommonInfoTabsRoot
          callDefaultSelectionFn
          selectedTab={this.props.selectedTab}
          onTabClickFn={this.props.onTabClickFn}
        />

        <div className="graph-container">
          <div className="graph-heading">
            {this.props.reportHeading}
          </div>
          {graphData}
        </div>
      </div>
    );
  }
}

RevenueReport.defaultProps = {
  items: {},
  reportHeading: null,
  selectedTab: 1,
  onTabClickFn() {},
  isSummaryLoading: false,
};

RevenueReport.propTypes = {
  items: PropTypes.object,
  reportHeading: PropTypes.element,
  selectedTab: PropTypes.number,
  onTabClickFn: PropTypes.func,
  isSummaryLoading: PropTypes.bool,
};

export default RevenueReport;
