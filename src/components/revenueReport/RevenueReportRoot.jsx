import React from 'react';
import PropTypes from 'prop-types';

import RevenueReport from './RevenueReport';

import CommonUtils from './../../utils/CommonUtils';
import DateUtils from './../../utils/DateUtils';
import DateSlider from './../datePicker/DateSlider';

class RevenueReportRoot extends React.Component {
  getDateSlider(selectedTab) {
    let dateSlider = null;

    if (selectedTab === 2) {
      const monthSelected = DateUtils.getDayMonthYear(this.props.reportEndDate).month;

      dateSlider = (
        <div className="date-picker graph-datepicker">
          <DateSlider
            selectorType={'M'}
            selectorSize={'F'}
            selectedVal={monthSelected}
            minVal={1}
            maxVal={12}
            onPreviousClick={this.props.onPreviousClick}
            onNextClick={this.props.onNextClick}
          />
        </div>
      );
    }

    return dateSlider;
  }

  getReportHeading() {
    let reportHeading = null;

    if (!CommonUtils.isEmpty(this.props.reportStartDate) && !CommonUtils.isEmpty(this.props.reportEndDate)) {
      reportHeading = (
        <span>
          {`${DateUtils.getDateInFormat(this.props.reportStartDate, DateUtils.dMY, ' ')} 
          - ${DateUtils.getDateInFormat(this.props.reportEndDate, DateUtils.dMY, ' ')}`}
        </span>
      );
    }

    return (<div><h4> Showing graph for </h4>{reportHeading}</div>);
  }

  render() {
    const tabsInfoItems = CommonUtils.getObjectValueIfEmpty(this.props.tabsInfo, 'items', {});
    const tabsInfoSelectedTab = CommonUtils.getObjectValueIfEmpty(this.props.tabsInfo, 'selectedTab', 1);
    const tabsInfoOnTabClickFn = CommonUtils.getObjectValueIfEmpty(this.props.tabsInfo, 'onTabClickFn', () => {});
    const reportHeading = tabsInfoSelectedTab === 2 ? this.getDateSlider(tabsInfoSelectedTab) : this.getReportHeading();

    return (
      <div className="main-container graph-section">
        <RevenueReport
          isSummaryLoading={this.props.isSummaryLoading}
          items={tabsInfoItems}
          selectedTab={tabsInfoSelectedTab}
          onTabClickFn={tabsInfoOnTabClickFn}
          reportHeading={reportHeading}
        />
      </div>
    );
  }
}

RevenueReportRoot.defaultProps = {
  tabsInfo: {},
  reportStartDate: '',
  reportEndDate: '',
  isSummaryLoading: false,
  onPreviousClick() {},
  onNextClick() {},
};

RevenueReportRoot.propTypes = {
  tabsInfo: PropTypes.shape({
    items: PropTypes.object,
    selectedTab: PropTypes.number,
    onTabClickFn: PropTypes.func.isRequired,
  }),
  reportStartDate: PropTypes.string,
  reportEndDate: PropTypes.string,
  isSummaryLoading: PropTypes.bool,
  onPreviousClick: PropTypes.func,
  onNextClick: PropTypes.func,
};

export default RevenueReportRoot;
