import React from 'react';
import PropTypes from 'prop-types';

import CommonUtils from './../../utils/CommonUtils';

class CustomerReviewsItem extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      showFullContent: false,
    };

    this.expand = this.expand.bind(this);
  }

  expand() {
    this.setState({ showFullContent: true });
  }

  render() {
    const ratingValue = { width: `${(this.props.rating * 100) / 5}%` };
    let seeMore = null;
    let content = this.props.review || '';

    if (CommonUtils.isEmpty(content.trim())) {
      content = 'Guest did not leave any comments for this review.';
    } else if (!this.state.showFullContent && this.props.review.length > this.props.lessContentLength) {
      content = `${this.props.review.substr(0, this.props.lessContentLength)}...`;
      seeMore = (<span className="see-more" role="button" onClick={this.expand}>See More</span>);
    }

    return (
      <div className="customer-review">
        <strong>{this.props.title}</strong>
        <div className="customer-review-star">
          <span className="customer-review-icon">
            <span style={ratingValue} />
          </span>
          {this.props.date}
        </div>
        <p>
          {content}
        </p>
        {seeMore}
      </div>
    );
  }
}

CustomerReviewsItem.defaultProps = {
  title: '',
  date: '',
  review: '',
  rating: 0,
  lessContentLength: 150,
};

CustomerReviewsItem.propTypes = {
  title: PropTypes.string.isRequired,
  date: PropTypes.string.isRequired,
  review: PropTypes.string.isRequired,
  rating: PropTypes.number.isRequired,
  lessContentLength: PropTypes.number,
};

export default CustomerReviewsItem;
