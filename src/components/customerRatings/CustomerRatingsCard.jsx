import React from 'react';
import PropTypes from 'prop-types';

import InfoBoxes from '../common/InfoBoxes';

class CustomerRatingsCard extends React.Component {
  render() {
    const cardClass = this.props.cardClass;
    const title = this.props.title;
    const items = this.props.items;

    return (
      <div className={cardClass}>
        <h3> {title} </h3>
        <InfoBoxes
          items={items}
        />
      </div>
    );
  }
}

CustomerRatingsCard.defaultProps = {
  cardClass: '',
  title: '',
  items: [],
};

CustomerRatingsCard.propTypes = {
  cardClass: PropTypes.string,
  title: PropTypes.string.isRequired,
  items: PropTypes.arrayOf(PropTypes.object).isRequired,
};

export default CustomerRatingsCard;
