import React from 'react';
import PropTypes from 'prop-types';

import CustomerRatingsCard from './CustomerRatingsCard';
import CustomerReviewsCard from './CustomerReviewsCard';

class CustomerRatings extends React.Component {
  render() {
    return (
      <div className="main-container">
        <CustomerRatingsCard
          cardClass={'infoboxes-container'}
          title={this.props.hotelRatings.heading}
          items={this.props.hotelRatings.items}
        />

        <CustomerRatingsCard
          cardClass={'infoboxes-container fab-hotels-infoboxes-container'}
          title={this.props.fabhotelRatings.heading}
          items={this.props.fabhotelRatings.items}
        />

        <CustomerReviewsCard
          cardClass={'infoboxes-container review-section'}
          title={this.props.customerReviews.heading}
          items={this.props.customerReviews.items}
          count={this.props.customerReviews.count}
        />
      </div>
    );
  }
}

CustomerRatings.defaultProps = {
  hotelRatings: {},
  fabhotelRatings: {},
  customerReviews: {},
};

CustomerRatings.propTypes = {
  hotelRatings: PropTypes.shape({
    heading: PropTypes.string,
    items: PropTypes.arrayOf(PropTypes.object),
  }),
  fabhotelRatings: PropTypes.shape({
    heading: PropTypes.string,
    items: PropTypes.arrayOf(PropTypes.object),
  }),
  customerReviews: PropTypes.shape({
    heading: PropTypes.string,
    items: PropTypes.arrayOf(PropTypes.object),
    count: PropTypes.number,
  }),
};

export default CustomerRatings;
