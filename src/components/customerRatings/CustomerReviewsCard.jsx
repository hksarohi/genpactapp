import React from 'react';
import PropTypes from 'prop-types';

import CommonUtils from './../../utils/CommonUtils';
import DateUtils from './../../utils/DateUtils';

import CustomerReviewsItem from './CustomerReviewsItem';

class CustomerReviewsCard extends React.Component {

  getCustomerReviews() {
    let counter = 0;
    const customerReviews = this.props.items.map((item) => {
      counter += 1;

      const title = CommonUtils.getObjectValueIfEmpty(item, 'name', null);
      const rating = CommonUtils.getObjectValueIfEmpty(item, 'rating', null);
      const date = DateUtils.getDateInFormat(CommonUtils.getObjectValueIfEmpty(item, 'date', ''), DateUtils.dMY, ' ');
      const review = CommonUtils.getObjectValueIfEmpty(item, 'body', null);

      return (
        <CustomerReviewsItem
          lessContentLength={145}
          key={counter}
          title={title}
          rating={rating}
          date={date}
          review={review}
        />
      );
    });
    return customerReviews;
  }

  render() {
    const customerReviews = this.getCustomerReviews();
    const cardClass = this.props.cardClass;
    const title = (
      <div>
        <h3>
          {this.props.title}
          <span> ({this.props.count} Reviews)</span>
        </h3>
      </div>);

    return (
      <div className={cardClass}>
        {title}
        {customerReviews}
      </div>
    );
  }
}

CustomerReviewsCard.defaultProps = {
  cardClass: '',
  title: '',
  count: 0,
  items: [],
};

CustomerReviewsCard.propTypes = {
  cardClass: PropTypes.string,
  title: PropTypes.string.isRequired,
  count: PropTypes.number.isRequired,
  items: PropTypes.arrayOf(PropTypes.object),
};

export default CustomerReviewsCard;
