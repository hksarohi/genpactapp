import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import HamBurger from './HamBurger';
import SideBar from './../sideBar/SideBar';

class Header extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      openSideBar: false,
      fixHeader: false,
    };

    this.openSideBarMenu = this.openSideBarMenu.bind(this);
    this.closeSideBarMenu = this.closeSideBarMenu.bind(this);
  }

  getPageTitleComponent() {
    let pageTitle = null;

    if (this.props.showLogo) {
      pageTitle = (<h2> {this.props.pageTitle} </h2>);
    } else {
      pageTitle = (<h1> {this.props.pageTitle} </h1>);
    }

    return pageTitle;
  }

  openSideBarMenu() {
    this.setState({
      openSideBar: true,
    });
  }

  closeSideBarMenu() {
    this.setState({
      openSideBar: false,
    });
  }

  render() {
    const logo = this.props.showLogo ? (<span className="logo" />) : '';
    const backBtn = this.props.showBackButton ? (<span role="button" className="back-arrow" onClick={this.props.onBackBtnClick} />) : '';
    const leftPortionClass = this.props.showLogo ? 'logo-wrapper clearfix' : '';
    const pageTitle = this.getPageTitleComponent();
    const hamBurger = this.props.showSideBar ? (<HamBurger onHamBurgerClick={this.openSideBarMenu} />) : '';
    const openSideBarMenu = (this.props.showSideBar && this.state.openSideBar);
    const sideBar = this.props.showSideBar ? (<SideBar openSideBar={openSideBarMenu} closeSideBarFn={this.closeSideBarMenu} />) : '';

    return (
      <div>
        <header className="header clearfix fixed-header" >
          <div className={leftPortionClass} >
            {backBtn}
            {logo}
            {pageTitle}
          </div>
          {hamBurger}
        </header>
        {sideBar}
      </div>
    );
  }
}

// Specifies the default values for props:
Header.defaultProps = {
  showBackButton: false,
  showLogo: false,
  showSideBar: false,
  pageTitle: null,
  onBackBtnClick() {},
};

// prop types validation
Header.propTypes = {
  showBackButton: PropTypes.bool,
  showLogo: PropTypes.bool,
  showSideBar: PropTypes.bool,
  pageTitle: PropTypes.string,
  onBackBtnClick: PropTypes.func,
};

export default connect(null)(Header);
