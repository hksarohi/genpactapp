import React from 'react';
import PropTypes from 'prop-types';

class ConfirmBox extends React.Component {

  render() {
    return (
      <div className={`confirm-box-cntr ${this.props.containerClass}`}>
        <h2>{this.props.title} </h2>
        <div className="confirm-box-wrapper clearfix">
          <div
            className={`confirm-box-cancel ${this.props.cancelBtnClass}`}
            role="button"
            onClick={this.props.onCancelClick}
          >
            {this.props.cancelBtnText}
          </div>

          <div
            className={`confirm-box-ok ${this.props.okBtnClass}`}
            role="button"
            onClick={this.props.onOkClick}
          >
            {this.props.okBtnText}
          </div>
        </div>
      </div>
    );
  }
}

// Specifies the default values for props:
ConfirmBox.defaultProps = {
  title: '',
  containerClass: '',
  cancelBtnClass: '',
  okBtnClass: '',
  cancelBtnText: '',
  okBtnText: '',
  onCancelClick() {},
  onOkClick() {},
};

// prop types validation
ConfirmBox.propTypes = {
  title: PropTypes.string,
  containerClass: PropTypes.string,
  cancelBtnClass: PropTypes.string,
  okBtnClass: PropTypes.string,
  cancelBtnText: PropTypes.string,
  okBtnText: PropTypes.string,
  onCancelClick: PropTypes.func,
  onOkClick: PropTypes.func,
};

export default ConfirmBox;
