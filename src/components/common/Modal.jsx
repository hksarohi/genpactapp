import React from 'react';
import PropTypes from 'prop-types';

import CommonHelper from './../../helpers/CommonHelper';

class Modal extends React.Component {
  constructor(props) {
    super(props);

    this.onCloseClick = this.onCloseClick.bind(this);
  }

  componentDidMount() {
    CommonHelper.disableBodyScroll();
  }

  componentWillUnmount() {
    CommonHelper.enableBodyScroll();
  }

  onCloseClick(event) {
    if (!this.modalContent.contains(event.target)) {
      this.props.closeFn();
    }
  }

  render() {
    return (
      <div
        role="button"
        onClick={this.onCloseClick}
      >
        <div ref={(modalContent) => { this.modalContent = modalContent; }} className="modal">
          {this.props.children}
        </div>

        <div className="overlay" />
      </div>
    );
  }
}

Modal.defaultProps = {
  closeFn() {},
  children: null,
};

Modal.propTypes = {
  closeFn: PropTypes.func,
  children: PropTypes.element,
};

export default Modal;
