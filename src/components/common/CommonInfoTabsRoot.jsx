import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import InfoTabs from './../common/InfoTabs';
import DatePicker from './../datePicker/DatePicker';
import DateUtils from './../../utils/DateUtils';
import Modal from './../common/Modal';

class CommonInfoTabsRoot extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedTab: this.props.selectedTab || 1,
      showCalenderOverlay: (this.props.selectedTab === 2),
    };

    this.onTabClick = this.onTabClick.bind(this);
    this.onCloseClickHandler = this.onCloseClickHandler.bind(this);
    this.onDateSelection = this.onDateSelection.bind(this);
  }

  componentDidMount() {
    // call tab click handler for default selected tab
    if (this.props.callDefaultSelectionFn) this.onTabClick(this.state.selectedTab);
  }

  onCloseClickHandler() {
    this.setState({
      selectedTab: 2,
      showCalenderOverlay: false,
    });
  }

  onDateSelection(year, month) {
    this.setState({
      selectedTab: 2,
      showCalenderOverlay: false,
    });

    const selectedDate = DateUtils.getInstance(`${year}-${month}-01`);

    const range = DateUtils.getMonthRangeDates(selectedDate);
    this.props.onTabClickFn(2, range.startDate, range.endDate);
  }

  onFirstTabSelect() {
    this.setState({
      selectedTab: 0,
      showCalenderOverlay: false,
    });

    const endDate = DateUtils.getDateInFormat(new Date(), DateUtils.yyyymmdd, '-');
    const startDate = DateUtils.getDateInFormat(DateUtils.add(endDate, { days: '-6' }), DateUtils.yyyymmdd, '-');

    this.props.onTabClickFn(0, startDate, endDate);
  }

  onSecondTabSelect() {
    this.setState({
      selectedTab: 1,
      showCalenderOverlay: false,
    });

    const today = new Date();
    const y = today.getFullYear();
    const m = today.getMonth() + 1;

    const startDate = DateUtils.getDateInFormat(DateUtils.getInstance(`${y}-${m}-01`), DateUtils.yyyymmdd, '-');
    const endDate = DateUtils.getDateInFormat(today, DateUtils.yyyymmdd, '-');

    this.props.onTabClickFn(1, startDate, endDate);
  }

  onThirdTabSelect() {
    this.setState({
      selectedTab: 2,
      showCalenderOverlay: true,
    });
  }

  /*
   | onTabClick for InfoTabs
   */
  onTabClick(id) {
    switch (id) {
      case 0: this.onFirstTabSelect(); break;
      case 1: this.onSecondTabSelect(); break;
      case 2: this.onThirdTabSelect(); break;
      default: break;
    }
  }

  getInfoTabs() {
    const tabs = [];

    if (this.props.showFirstTab) {
      tabs.push({
        id: 0,
        title: 'Last 7 Days',
        onTabClick: this.onTabClick,
      });
    }

    if (this.props.showSecondTab) {
      tabs.push({
        id: 1,
        title: 'MTD',
        onTabClick: this.onTabClick,
      });
    }

    if (this.props.showThirdTab) {
      tabs.push({
        id: 2,
        title: 'Select Month',
        onTabClick: this.onTabClick,
      });
    }

    return tabs;
  }

  render() {
    const tabItems = this.getInfoTabs();
    const calendar = this.state.showCalenderOverlay ? (
      <Modal closeFn={this.onCloseClickHandler}>
        <DatePicker onDateSelection={this.onDateSelection} />
      </Modal>
    ) : null;

    return (
      <div>
        <InfoTabs
          items={tabItems}
          selectedTab={this.state.selectedTab}
        />

        {calendar}

      </div>
    );
  }
}

// Specifies the default values for props:
CommonInfoTabsRoot.defaultProps = {
  onTabClickFn() {},
  selectedTab: 1,
  callDefaultSelectionFn: false,
  showFirstTab: true,
  showSecondTab: true,
  showThirdTab: true,
};

// prop types validation
CommonInfoTabsRoot.propTypes = {
  onTabClickFn: PropTypes.func,
  selectedTab: PropTypes.number,
  // call handler for default selected tab
  callDefaultSelectionFn: PropTypes.bool,
  showFirstTab: PropTypes.bool,
  showSecondTab: PropTypes.bool,
  showThirdTab: PropTypes.bool,
};

export default connect(null)(CommonInfoTabsRoot);
