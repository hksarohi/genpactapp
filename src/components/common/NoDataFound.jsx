import React from 'react';

class NoDataFound extends React.Component {
  render() {
    return (
      <div className="empty-screen-container">
        <i className="generic-empty-icon" />
        <h2> Sorry, no data found </h2>
        <p> Either there is no data to show or try refreshing to view! </p>
      </div>
    );
  }
}

NoDataFound.defaultProps = {

};

NoDataFound.propTypes = {

};


export default NoDataFound;
