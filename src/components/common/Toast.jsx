import React from 'react';
import PropTypes from 'prop-types';

class Toast extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      visible: true,
    };
  }

  componentWillReceiveProps(nextProps) {
    const timeout = this.props.timeout * 1000;

    setTimeout(() => {
      this.setState({ visible: false });
    }, timeout);
  }

  render() {
    const toastClass = !this.state.visible ? 'toast-hidden' : '';

    return (
      <div className={`toast ${toastClass}`}>
        <div className="toast-msg">
          {this.props.msg}
        </div>
      </div>
    );
  }
}

// Specifies the default values for props:
Toast.defaultProps = {
  msg: '',
  // in seconds
  timeout: 5,
};

// prop types validation
Toast.propTypes = {
  msg: PropTypes.string,
  timeout: PropTypes.number,
};

export default Toast;
