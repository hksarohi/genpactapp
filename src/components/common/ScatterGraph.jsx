import React from 'react';
import PropTypes from 'prop-types';
import Chart from 'chart.js';

class ScatterGraph extends React.Component {
  componentDidMount() {
    this.drawChart(this.props.data);
  }

  drawChart(data) {
    const ctx = document.getElementById('scatter-chart').getContext('2d');

    /** * Gradient ***/
    const gradient = ctx.createLinearGradient(0, 0, 0, 400);
    gradient.addColorStop(0, 'rgba(30, 37, 74, 1)');
    gradient.addColorStop(0.60, 'rgba(0, 117, 255, 0.6)');
    gradient.addColorStop(0.80, 'rgba(0, 117, 255, 0.4)');
    gradient.addColorStop(0.90, 'rgba(0, 117, 255, 0.2)');
    gradient.addColorStop(1, 'rgba(0, 117, 255, 0.1)');

    const graph = new Chart(ctx, {
      type: 'scatter',
      data: {
        datasets: [{
          fillColor: gradient,
          pointStyle: 'circle',
          pointBackgroundColor: '#fff',
          pointBorderColor: '#d3d3d3',
          pointRadius: 2.5,
          data,
          fill: 'start',
          backgroundColor: gradient,
          borderColor: '#a3d6d2',
          borderWidth: 1,
        }],
      },
      options: {
        legend: { display: false },
        responsive: true,
        pointDotStrokeWidth: 4,
        events: ['click'],
        tooltips: false,
        scales: {
          yAxes: [
            {
              ticks: {
                stepSize: this.props.yTicksInterval,
                callback: this.props.yTickFormat,
                maxTicksLimit: this.props.yTicksCount,
                fontColor: '#999',
                fontSize: 12,
                min: this.props.yTicksMinVal,
                max: this.props.yTicksMaxVal,
              },
            },
          ],
          xAxes: [
            {
              ticks: {
                callback: this.props.xTickFormat,
                maxTicksLimit: this.props.xTicksCount,
                fontColor: '#999',
                fontSize: 12,
              },
              type: 'time',
              time: {
                unit: 'day',
                unitStepSize: this.props.xTicksInterval,
                displayFormats: {
                  millisecond: 'DD MMM',
                  second: 'DD MMM',
                  minute: 'DD MMM',
                  hour: 'DD MMM',
                  day: 'DD MMM',
                  week: 'DD MMM',
                  month: 'DD MMM',
                  quarter: 'DD MMM',
                  year: 'DD MMM',
                },
              },
            },
          ],
        },
      },
    });

    return this;
  }

  render() {
    return (
      <div><canvas id="scatter-chart" width="320" height="300" /></div>
    );
  }
}

ScatterGraph.defaultProps = {
  xTicksInterval: 0,
  yTicksInterval: 0,
  xTicksCount: 0,
  yTicksCount: 0,
  yTicksMinVal: undefined,
  yTicksMaxVal: undefined,
  xTickFormat(label) { return label; },
  yTickFormat(label) { return label; },
  data: [],
};

ScatterGraph.propTypes = {
  xTicksInterval: PropTypes.number,
  yTicksInterval: PropTypes.number,
  xTicksCount: PropTypes.number,
  yTicksCount: PropTypes.number,
  yTicksMinVal: PropTypes.number,
  yTicksMaxVal: PropTypes.number,
  xTickFormat: PropTypes.func,
  yTickFormat: PropTypes.func,
  data: PropTypes.arrayOf(PropTypes.object),
};

export default ScatterGraph;
