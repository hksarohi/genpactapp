import React from 'react';
import PropTypes from 'prop-types';

import NoDataFound from './../common/NoDataFound';
import Loader from './../../components/common/Loader';

import CommonUtils from './../../utils/CommonUtils';

class InfoTable extends React.Component {

  getHeadings() {
    let counter = 0;

    const headings = this.props.headings.map((item) => {
      counter += 1;
      return (<th key={counter}>{item}</th>);
    });

    return (<tr>{headings}</tr>);
  }

  getRowData(item) {
    let iKeyCounter = 0;

    const rowData = this.props.itemKeys.map((iKey) => {
      iKeyCounter += 1;

      let itemValue = CommonUtils.getObjectValue(item, iKey, '');
      itemValue = this.props.formatRowValue(iKey, itemValue);

      return (<td key={iKeyCounter}>{itemValue}</td>);
    });

    return rowData;
  }

  getRows() {
    let rowCounter = 0;
    const rows = this.props.items.map((item) => {
      rowCounter += 1;
      const rowData = this.getRowData(item);

      return (<tr key={rowCounter}>{rowData}</tr>);
    });

    return rows;
  }

  render() {
    const headings = this.getHeadings();
    const rows = this.getRows();

    let reportData = null;
    if (this.props.isSummaryLoading) {
      reportData = (<Loader type={2} extraClassName="loader-blue" />);
    } else if (CommonUtils.isEmpty(rows)) {
      reportData = (<NoDataFound />);
    } else {
      reportData = (
        <div className="infotable-content">
          <table>
            <thead>
              {headings}
            </thead>
            <tbody>
              {rows}
            </tbody>
          </table>
        </div>
      );
    }

    return (
      <div>
        { reportData }
      </div>
    );
  }
}

InfoTable.defaultProps = {
  isSummaryLoading: false,
  // headings and itemsKeys must be in synch
  headings: [],
  itemKeys: [],
  items: [],
  formatRowValue(key, item) { return item; },
};

InfoTable.propTypes = {
  isSummaryLoading: PropTypes.bool,
  headings: PropTypes.arrayOf(PropTypes.string),
  itemKeys: PropTypes.arrayOf(PropTypes.string),
  items: PropTypes.arrayOf(PropTypes.object),
  formatRowValue: PropTypes.func,
  containerClass: PropTypes.string,
};

export default InfoTable;
