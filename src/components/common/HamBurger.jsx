import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

class HamBurger extends React.Component {
  render() {
    return (
      <div className="main-nav" role="button" onClick={this.props.onHamBurgerClick} >
        <span className="top" />
        <span className="middle" />
        <span className="bottom" />
      </div>
    );
  }
}

// Specifies the default values for props:
HamBurger.defaultProps = {
  onHamBurgerClick() {},
};

// prop types validation
HamBurger.propTypes = {
  onHamBurgerClick: PropTypes.func,
};

export default connect(null)(HamBurger);
