import React from 'react';
import PropTypes from 'prop-types';

class Loader extends React.Component {
  render() {
    // full page loader
    if (this.props.type === 0) return (<div className={`loader ${this.props.extraClassName}`} />);
    // progress bar at top of page
    if (this.props.type === 1) return (<div className={`loader-progressbar ${this.props.extraClassName}`} />);
    // loader with dots popping
    if (this.props.type === 2) {
      return (
        <div className={`loading-dots ${this.props.extraClassName}`}>
          <div className="loading-dots--dot" />
          <div className="loading-dots--dot" />
          <div className="loading-dots--dot" />
        </div>
      );
    }

    return (
      <div />
    );
  }
}

// Specifies the default values for props:
Loader.defaultProps = {
  type: 0,
  extraClassName: '',
};

// prop types validation
Loader.propTypes = {
  type: PropTypes.number,
  extraClassName: PropTypes.string,
};

export default Loader;
