import React from 'react';
import PropTypes from 'prop-types';

class InfoTabs extends React.Component {

  render() {
    const infotabClass = `infotabs infotab-count-${this.props.items.length} clearfix`;
    const infoTabs = this.props.items.map(item => (
      <a
        key={item.id}
        className={item.id === this.props.selectedTab ? 'selected' : null}
        tabIndex={item.id === this.props.selectedTab ? 0 : -1}
        onClick={() => item.onTabClick(item.id)}
        role="link"
      >
        {item.title}
      </a>),
    );

    return (
      <div className={infotabClass}>
        {infoTabs}
      </div>
    );
  }
}

InfoTabs.defaultProps = {
  items: [],
  selectedTab: 1,
};

InfoTabs.propTypes = {
  items: PropTypes.arrayOf(PropTypes.object).isRequired,
  selectedTab: PropTypes.number,

};

export default InfoTabs;
