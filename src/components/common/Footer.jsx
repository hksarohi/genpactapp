import React from 'react';
import PropTypes from 'prop-types';

class Footer extends React.Component {
  render() {
    const footerClass = (this.props.goDark) ? 'footer footer-dark ' : 'footer';
    return (
      <footer className={footerClass}>
        <h4> For any queries/concerns, </h4>
        <p> please drop us a line at ceo@fabhotels.com <br />
          or contact your FabHotels' city manager. </p>
      </footer>
    );
  }
}

Footer.defaultProps = {
  goDark: false,
};

Footer.propTypes = {
  goDark: PropTypes.bool,
};


export default Footer;
