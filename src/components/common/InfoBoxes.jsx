import React from 'react';
import PropTypes from 'prop-types';

import CommonUtils from './../../utils/CommonUtils';

class InfoBoxes extends React.Component {
  getInfoBoxes() {
    let counter = 0;
    const infoBoxesData = this.props.items.map((item) => {
      counter += 1;

      const title = CommonUtils.getObjectValueIfEmpty(item, 'title', null);
      const subtext = CommonUtils.getObjectValueIfEmpty(item, 'subtext', null);
      const count = CommonUtils.getObjectValueIfEmpty(item, 'count', null);
      const logo = CommonUtils.getObjectValueIfEmpty(item, 'showLogo', false) ? <span className="fab-logo" /> : '';

      return (
        <div key={counter} className="infobox animated fadeInLeft">
          <strong className="infobox-detail">
            {title}
          </strong>
          <span className="infobox-count">
            {count}
          </span>
          <span className="infobox-subtext">
            {logo}
            {subtext}
          </span>
        </div>
      );
    });

    return infoBoxesData;
  }

  render() {
    const infoBoxes = this.getInfoBoxes();

    return (
      <div className="rating-section clearfix">
        {infoBoxes}
      </div>
    );
  }
}

InfoBoxes.defaultProps = {
  items: [],
};

InfoBoxes.propTypes = {
  items: PropTypes.arrayOf(PropTypes.object),
};

export default InfoBoxes;
