import React from 'react';
import PropTypes from 'prop-types';

class ScrollToTop extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      showTopButton: false,
    };

    this.handleButtonVisibility = this.handleButtonVisibility.bind(this);
    this.onButtonClick = this.onButtonClick.bind(this);
  }

  componentDidMount() {
    window.addEventListener('scroll', this.handleButtonVisibility);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleButtonVisibility);
  }

  onButtonClick() {
    this.goTop();
  }

  handleButtonVisibility() {
    const { heightFromTop } = this.props;

    if (document.body.scrollTop > heightFromTop || document.documentElement.scrollTop > heightFromTop) {
      this.setState({ showTopButton: true });
    } else {
      this.setState({ showTopButton: false });
    }
  }

  goTop() {
    const scrollToTop = window.setInterval(() => {
      const pos = window.pageYOffset;
      if (pos > 0) {
        window.scrollTo(0, pos - 80); // how far to scroll on each step
      } else {
        window.clearInterval(scrollToTop);
      }
    }, 1);

    return this;
  }

  render() {
    if (!this.state.showTopButton) {
      return (<div />);
    }

    return (
      <div onClick={this.onButtonClick} className={this.props.buttonClass}>
        Scroll To Top
      </div>
    );
  }
}

ScrollToTop.defaultProps = {
  buttonClass: 'scroll-top-btn',
  heightFromTop: 800,
};

ScrollToTop.propTypes = {
  buttonClass: PropTypes.string,
  heightFromTop: PropTypes.number,
};

export default ScrollToTop;
