import React from 'react';
import PropTypes from 'prop-types';

import LoginForm from './LoginForm';
import LoginOtpForm from './LoginOtpForm';

class LoginRoot extends React.Component {
  render() {
    let loginComponent = (
      <LoginForm
        isLoggedIn={this.props.isLoggedIn}
        isFormSubmitting={this.props.isLoginFormSubmitting}
        onLoginSubmit={this.props.onLoginSubmit}
      />
    );

    // show the otp form (one of Login or OTP form will be shown at a time)
    if (this.props.showOtpForm) {
      loginComponent = (
        <LoginOtpForm
          invalidOtpCount={this.props.invalidOtpCount}
          isFormSubmitting={this.props.isOTPFormSubmitting}
          isResendingOtp={this.props.isResendingOtp}
          onLoginOtpSubmit={this.props.onLoginOtpSubmit}
          onLoginOtpResend={this.props.onLoginOtpResend}
          onLoginOtpCancel={this.props.onLoginOtpCancel}
          onOtpLimitExceeded={this.props.onOtpLimitExceeded}
        />
      );
    }

    return (
      <div className="login">
        <span className="logo" />
        <h2>Wayne - One stop to know your <br />FabHotel performance</h2>
        { loginComponent }
      </div>
    );
  }
}

LoginRoot.defaultProps = {
  showOtpForm: false,
  isLoggedIn: false,
  invalidOtpCount: 0,
  isLoginFormSubmitting: false,
  isOTPFormSubmitting: false,
  isResendingOtp: false,
  onLoginSubmit() {},
  onLoginOtpSubmit() {},
  onLoginOtpCancel() {},
  onLoginOtpResend() {},
  onOtpLimitExceeded() {},
};

LoginRoot.propTypes = {
  showOtpForm: PropTypes.bool,
  isLoggedIn: PropTypes.bool,
  invalidOtpCount: PropTypes.number,
  isLoginFormSubmitting: PropTypes.bool,
  isOTPFormSubmitting: PropTypes.bool,
  isResendingOtp: PropTypes.bool,
  onLoginSubmit: PropTypes.func.isRequired,
  onLoginOtpSubmit: PropTypes.func.isRequired,
  onLoginOtpCancel: PropTypes.func.isRequired,
  onLoginOtpResend: PropTypes.func.isRequired,
  onOtpLimitExceeded: PropTypes.func.isRequired,
};

export default LoginRoot;
