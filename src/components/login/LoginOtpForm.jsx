import React from 'react';
import PropTypes from 'prop-types';
import Loader from './../common/Loader';
import CommonUtils from './../../utils/CommonUtils';
import Validator from './../../utils/Validator';

class LoginOtpForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      submitEnabled: true,
      errors: {
        otp: null,
      },
      resendOtpMsg: '',
    };

    this.onFormSubmit = this.onFormSubmit.bind(this);
    this.onKeyPress = this.onKeyPress.bind(this);
    this.removeErrorMsg = this.removeErrorMsg.bind(this);
    this.onResendOtp = this.onResendOtp.bind(this);
    this.onCancelOtp = this.onCancelOtp.bind(this);
    this.onOtpLimitExceeded = this.onOtpLimitExceeded.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (!nextProps.isFormSubmitting && nextProps.invalidOtpCount > 0) {
      if (nextProps.invalidOtpCount === nextProps.maxOtpAttempts) {
        this.onOtpLimitExceeded();
        return;
      }

      if (this.props.invalidOtpCount !== nextProps.invalidOtpCount) {
        this.setState({
          errors: { otp: `${'Please enter a valid OTP. Only '}${nextProps.maxOtpAttempts - nextProps.invalidOtpCount}${' more attempts left.'}` },
          resendOtpMsg: '',
        });
      }
    }
  }

  onResendOtp() {
    this.props.onLoginOtpResend();
    this.setState({
      errors: { otp: null },
      resendOtpMsg: 'An OTP has been sent to your phone',
    });
  }

  onCancelOtp() {
    this.props.onLoginOtpCancel();
  }

  onOtpLimitExceeded() {
    this.props.onOtpLimitExceeded();
  }

  onFormSubmit() {
    const formData = {
      otp: this.otp.value,
    };

    if (this.validateInput(formData)) {
      this.props.onLoginOtpSubmit(formData);
    }
  }

  onKeyPress(event) {
    if (!(event.which >= 48 && event.which <= 57)) {
      event.preventDefault();
      event.stopPropagation();
    }
  }

  validateInput(data) {
    if (CommonUtils.isEmpty(data.otp)) {
      this.setState({ errors: { otp: 'Please enter OTP' } });
      return false;
    }

    if (!Validator.isNumber(data.otp)) {
      this.setState({ errors: { otp: 'OTP must be a number' },
      });
      return false;
    }

    return true;
  }

  removeErrorMsg() {
    this.setState({
      errors: { otp: null },
      resendOtpMsg: '',
    });
  }

  render() {
    const buttonClass = (this.state.submitEnabled ? 'login-link' : 'login-link login-link-disabled');
    const textMessage = (!this.state.resendOtpMsg.isEmpty ? (<p className="input-feedback-message">{this.state.resendOtpMsg}</p>) : '');

    let submitButton = (
      <button
        onClick={this.onFormSubmit}
        className={buttonClass}
      >SUBMIT</button>
    );

    if (this.props.isFormSubmitting || this.props.isResendingOtp) {
      submitButton = (<button className="login-link"><Loader type={2} /></button>);
    }

    return (
      <div className="login-otp-form animated slideInRight clearfix">
        <div className="form-wrap">
          <label htmlFor="otp">Enter the OTP we have sent to your mobile number</label>
          <div className="form-wrap-otp">
            <input
              type="tel"
              pattern="[0-9]*"
              tabIndex={0}
              ref={(input) => { this.otp = input; }}
              autoComplete="off"
              placeholder=""
              onKeyPress={this.onKeyPress}
              onFocus={this.removeErrorMsg}
            />
            <a role="link" className="resend-otp-link" onClick={this.onResendOtp}>Resend OTP</a>
          </div>

        </div>
        {submitButton}
        <p className="field-error">{this.state.errors.otp}</p>
        {textMessage}
        {/* <a role="link" className="abort-otp-link" onClick={this.onCancelOtp}>Cancel</a>*/}
      </div>
    );
  }
}

LoginOtpForm.defaultProps = {
  onLoginOtpSubmit() {},
  onLoginOtpCancel() {},
  onLoginOtpResend() {},
  onOtpLimitExceeded() {},
  isFormSubmitting: false,
  isResendingOtp: false,
  invalidOtpCount: 0,
  maxOtpAttempts: 5,
};

LoginOtpForm.propTypes = {
  onLoginOtpSubmit: PropTypes.func.isRequired,
  onLoginOtpCancel: PropTypes.func.isRequired,
  onLoginOtpResend: PropTypes.func.isRequired,
  onOtpLimitExceeded: PropTypes.func.isRequired,
  isFormSubmitting: PropTypes.bool,
  isResendingOtp: PropTypes.bool,
  invalidOtpCount: PropTypes.number,
  maxOtpAttempts: PropTypes.number,
};

export default LoginOtpForm;
