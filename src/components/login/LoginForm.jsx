import React from 'react';
import PropTypes from 'prop-types';
import Loader from './../common/Loader';
import CommonUtils from './../../utils/CommonUtils';
import AuthenticationHelper from './../../helpers/AuthenticationHelper';

class LoginForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      submitEnabled: true,
      errors: {
        mobile: null,
      },
    };

    this.onFormSubmit = this.onFormSubmit.bind(this);
    this.onKeyPress = this.onKeyPress.bind(this);
    this.removeErrorMsg = this.removeErrorMsg.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    /**
     * If we had submitted form (this.props.isFormSubmitting) and
     * now the form has submitted (!nextProps.isFormSubmitting) and
     * still the person is not logged in (!nextProps.isLoggedIn)
     *
     * Means Invalid credentials. No matter what the reason was.
     */
    if (this.props.isFormSubmitting && !nextProps.isFormSubmitting && !nextProps.isLoggedIn) {
      this.setState({
        submitEnabled: false,
        errors: { mobile: `${'We do not have this mobile number in our records. Please enter the number shared with the city manager. In case of issues, please write to ceo@fabhotels.com'}` },
      });
    }
  }

  onFormSubmit() {
    const formData = {
      isoCode: this.isoCode.value,
      countryCode: CommonUtils.getNumber(this.countryCode.value),
      mobile: CommonUtils.getNumber(this.mobile.value),
    };

    if (this.validateInput(formData)) {
      this.props.onLoginSubmit(formData);
    }
  }

  onKeyPress(event) {
    if (!(event.which >= 48 && event.which <= 57)) {
      event.preventDefault();
      event.stopPropagation();
    }
  }

  validateInput(data) {
    if (CommonUtils.isEmpty(data.mobile)) {
      this.setState({ errors: { mobile: 'Please enter your mobile number' } });
      return false;
    }

    return true;
  }

  removeErrorMsg() {
    this.setState({
      submitEnabled: true,
      errors: { mobile: null },
    });
  }

  render() {
    const countryClass = (this.state.submitEnabled ? 'field-country' : 'field-country field-country-disabled');
    const mobileClass = (this.state.submitEnabled ? 'field-mobile' : 'field-mobile field-mobile-disabled');
    const buttonClass = (this.state.submitEnabled ? 'login-link' : 'login-link login-link-disabled');
    const textMessage = (!this.state.errors.mobile ? (<h4>Mobile number should be same as provided to the city manager.</h4>) : '');

    let submitButton = (
      <button
        tabIndex={0}
        onClick={this.onFormSubmit}
        className={buttonClass}
        disabled={!this.state.submitEnabled}
      >REQUEST OTP</button>
    );

    if (this.props.isFormSubmitting) {
      submitButton = (<button className="login-link"><Loader type={2} /></button>);
    }

    return (
      <div className="login-form animated slideInRight">
        <div className="form-wrap">
          <input
            type="hidden"
            tabIndex={-1}
            ref={(input) => { this.isoCode = input; }}
            value={'IN'}
          />
          <label htmlFor="mobile">Please authenticate your mobile number using One Time Password (OTP).</label>
          <input
            type="tel"
            pattern="[0-9]*"
            name="mobile"
            className={countryClass}
            tabIndex={-1}
            ref={(input) => { this.countryCode = input; }}
            value={'+91-'}
            readOnly
          />
          <input
            type="tel"
            pattern="[0-9]*"
            name="mobile"
            className={mobileClass}
            tabIndex={0}
            ref={(input) => { this.mobile = input; }}
            autoComplete="off"
            placeholder=""
            defaultValue={AuthenticationHelper.getMobileNumber()}
            onKeyPress={this.onKeyPress}
            onFocus={this.removeErrorMsg}
          />
        </div>
        {submitButton}
        <p className="field-error">{this.state.errors.mobile}</p>
        {textMessage}
      </div>
    );
  }
}

LoginForm.defaultProps = {
  onLoginSubmit() {},
  isLoggedIn: false,
  isFormSubmitting: false,
};

LoginForm.propTypes = {
  onLoginSubmit: PropTypes.func.isRequired,
  isLoggedIn: PropTypes.bool,
  isFormSubmitting: PropTypes.bool,
};

export default LoginForm;
