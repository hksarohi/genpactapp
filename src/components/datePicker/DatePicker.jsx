import React from 'react';
import PropTypes from 'prop-types';

import { months } from './../../constants/DateConstants';
import DateSlider from './DateSlider';

class DatePicker extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedYear: this.props.selectYear,
    };

    this.onPreviousYearClick = this.onPreviousYearClick.bind(this);
    this.onNextYearClick = this.onNextYearClick.bind(this);
    this.onDateSelection = this.onDateSelection.bind(this);
  }

  onPreviousYearClick(selectedYear) {
    this.setState({ selectedYear });
  }

  onNextYearClick(selectedYear) {
    this.setState({ selectedYear });
  }

  onDateSelection(event) {
    this.props.onDateSelection(this.state.selectedYear, event.currentTarget.getAttribute('data-month-id'));
  }

  getMonths() {
    const monthItems = months.map(month => (
      <li key={month.id}>
        <div
          role="button"
          tabIndex="-1"
          onClick={this.onDateSelection}
          data-month-id={month.id}
        >
          { (this.props.useShortName) ? month.shortName : month.fullName }
        </div>
      </li>
    ));
    return monthItems;
  }

  render() {
    return (
      <div className="date-picker">
        <div className="date-picker-header">
          <h2> { this.props.title } </h2>
        </div>

        <DateSlider
          minVal={this.props.minYear}
          maxVal={this.props.maxYear}
          selectedVal={this.state.selectedYear}
          onPreviousClick={this.onPreviousYearClick}
          onNextClick={this.onNextYearClick}
        />

        <ul className="clearfix select-month">
          { this.getMonths() }
        </ul>
      </div>
    );
  }
}

DatePicker.defaultProps = {
  title: 'Select Month',
  useShortName: false,
  selectYear: 2017,
  minYear: 2012,
  maxYear: 2017,
  onDateSelection() {},
};

DatePicker.propTypes = {
  title: PropTypes.string,
  useShortName: PropTypes.bool,
  selectYear: PropTypes.number,
  minYear: PropTypes.number,
  maxYear: PropTypes.number,
  onDateSelection: PropTypes.func,
};

export default DatePicker;
