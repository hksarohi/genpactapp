import React from 'react';
import PropTypes from 'prop-types';

import { months, weeks } from './../../constants/DateConstants';
import CommonUtils from './../../utils/CommonUtils';

class DateSlider extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      selectedVal: this.props.selectedVal || this.props.maxVal,
    };

    this.onPreviousClick = this.onPreviousClick.bind(this);
    this.onNextClick = this.onNextClick.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.selectedVal !== nextProps.selectedVal) {
      this.setState({ selectedVal: nextProps.selectedVal });
    }
  }

  onPreviousClick() {
    if (this.state.selectedVal > this.props.minVal) {
      let selectVal = this.state.selectedVal;
      selectVal -= 1;

      this.setState({ selectedVal: selectVal });

      this.props.onPreviousClick(selectVal);
    }
  }

  onNextClick() {
    if (this.state.selectedVal < this.props.maxVal) {
      let selectVal = this.state.selectedVal;
      selectVal += 1;

      this.setState({ selectedVal: selectVal });
      this.props.onNextClick(selectVal);
    }
  }

  getDisplayText() {
    const { selectedVal } = this.state;

    // for week names
    if (this.props.selectorType === 'W') {
      if (this.props.selectorSize === 'S') {
        return CommonUtils.getObjectValue(weeks, `${selectedVal - 1}.shortName`, selectedVal);
      }

      return CommonUtils.getObjectValue(weeks, `${selectedVal - 1}.fullName`, selectedVal);
    }

    // for month names
    if (this.props.selectorType === 'M') {
      if (this.props.selectorSize === 'S') {
        return CommonUtils.getObjectValue(months, `${selectedVal - 1}.shortName`, selectedVal);
      }

      return CommonUtils.getObjectValue(months, `${selectedVal - 1}.fullName`, selectedVal);
    }

    return selectedVal;
  }

  render() {
    const previousIconClass = (this.state.selectedVal > this.props.minVal) ? '' : ' disabled';
    const nextIconClass = (this.state.selectedVal < this.props.maxVal) ? '' : ' disabled';
    const displayText = this.getDisplayText();

    return (
      <div className="clearfix select-year">
        <div
          role="button"
          tabIndex="-1"
          className={`select-arrow select-prev${previousIconClass}`}
          onClick={this.onPreviousClick}
        />
        <div className="select-data">
          { displayText }
        </div>
        <div
          role="button"
          tabIndex="-1"
          className={`select-arrow select-next${nextIconClass}`}
          onClick={this.onNextClick}
        />
      </div>
    );
  }
}

DateSlider.defaultProps = {
  selectorType: '',
  selectorSize: '',
  selectedVal: 0,
  minVal: 0,
  maxVal: 0,
  onPreviousClick() {},
  onNextClick() {},
};

DateSlider.propTypes = {
  selectorType: PropTypes.string, // M => Month Names, W => Week Names
  selectorSize: PropTypes.string, // S => Small, F => Large
  selectedVal: PropTypes.number,
  minVal: PropTypes.number,
  maxVal: PropTypes.number,
  onPreviousClick: PropTypes.func,
  onNextClick: PropTypes.func,
};

export default DateSlider;
