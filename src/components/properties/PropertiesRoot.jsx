import React from 'react';
import PropTypes from 'prop-types';
import Slider from 'react-slick';
import PropertyItem from './PropertyItem';

class PropertiesRoot extends React.Component {
  getProperties() {
    let counter = 0;
    const loginProperties = this.props.hotels.map((item) => {
      counter += 1;

      return (
        <div key={counter}>
          <PropertyItem onPropertySelect={this.props.onPropertySelect} hotel={item} />
        </div>
      );
    });

    return loginProperties;
  }

  render() {
    const loginProperties = this.getProperties();
    let propertySlider = null;
    let heading = null;

    const enableSwipe = loginProperties.length !== 1;

    if (loginProperties.length > 0) {
      const settings = {
        dots: false,
        infinite: false,
        arrows: false,
        speed: 500,
        swipe: enableSwipe,
        slidesToShow: 2.52,
      };

      propertySlider = (<Slider {...settings} >{loginProperties}</Slider>);
      heading = (<h3> My FabHotels </h3>);
    } else if (!this.props.isHotelsLoading) {
      propertySlider = (
        <div className="properties-section">
          <h3> Thanks for logging in! </h3>
          <p> Your property will show in this space soon </p>
        </div>
      );
    }

    return (
      <div className="homepage-slider-wrapper animated slideInRight">
        {heading}
        {propertySlider}
      </div>
    );
  }
}

PropertiesRoot.defaultProps = {
  onPropertySelect() {},
  hotels: [],
  isHotelsLoading: false,
};

PropertiesRoot.propTypes = {
  onPropertySelect: PropTypes.func.isRequired,
  hotels: PropTypes.array,
  isHotelsLoading: PropTypes.bool,
};

export default PropertiesRoot;
