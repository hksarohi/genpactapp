import React from 'react';
import PropTypes from 'prop-types';

import { assetUrl } from './../../constants/Constants';

class PropertyItem extends React.Component {
  constructor(props) {
    super(props);

    this.onSelectItem = this.onSelectItem.bind(this);
  }

  onSelectItem() {
    this.props.onPropertySelect(this.props.hotel);
  }

  render() {
    const { hotel } = this.props;
    return (
      <div className="homepage-slider">
        <div
          role="button"
          tabIndex={0}
          onClick={this.onSelectItem}
        >
          <div className="slider-img">
            <img src={assetUrl.IMAGE_BASE_URL + hotel.propertyUrl} alt={hotel.propertyName} />
          </div>
          <div className="slider-caption">
            {hotel.propertyName}
          </div>
        </div>
      </div>
    );
  }
}

PropertyItem.defaultProps = {
  onPropertySelect() {},
  hotel: {},
};

PropertyItem.propTypes = {
  onPropertySelect: PropTypes.func.isRequired,
  hotel: PropTypes.object,
};

export default PropertyItem;
