import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import Header from './../components/common/Header';
import Footer from './../components/common/Footer';

import OccupancyReportRoot from './../components/occupancyReport/OccupancyReportRoot';

import { fetchOccupancyReportSummary, storeResetOccupancyReportSummary } from './../actions/OccupancyReportAction';

import { pageTitle } from './../constants/Constants';
import OccupancyReportHelper from './../helpers/OccupancyReportHelper';
import DateUtils from './../utils/DateUtils';
import CommonHelper from './../helpers/CommonHelper';
import ScrollToTop from './../components/common/ScrollToTop';

class OccupancyReportPage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedTab: 1,
      reportStartDate: null,
      reportEndDate: null,
    };

    this.onTabClick = this.onTabClick.bind(this);
    this.onBackBtnClick = this.onBackBtnClick.bind(this);
    this.onReportMonthSelection = this.onReportMonthSelection.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    // This will be called whenever we change hotel in side bar
    const oldSelectedHotel = CommonHelper.getSelectedHotelId(this.props.selectedHotel);
    const newSelectedHotel = CommonHelper.getSelectedHotelId(nextProps.selectedHotel);

    if (oldSelectedHotel !== newSelectedHotel) {
      this.props.fetchOccupancyReportSummary(
        newSelectedHotel, this.state.reportStartDate, this.state.reportEndDate);
    }
  }

  componentWillUnmount() {
    this.props.storeResetOccupancyReportSummary();
  }

  onBackBtnClick() {
    this.context.router.history.push('/dashboard');
  }

  onReportMonthSelection(month) {
    const y = DateUtils.getDayMonthYear(this.state.reportEndDate).year;
    const m = month;

    const selectedDate = DateUtils.getInstance(`${y}-${m}-01`);
    const range = DateUtils.getMonthRangeDates(selectedDate);

    this.onTabClick(2, range.startDate, range.endDate);
  }

  onTabClick(id, startDate, endDate) {
    this.setState({
      selectedTab: id,
      reportStartDate: startDate,
      reportEndDate: endDate,
    });

    const propertyId = CommonHelper.getSelectedHotelId(this.props.selectedHotel);

    if (propertyId > 0) {
      this.props.fetchOccupancyReportSummary(propertyId, startDate, endDate);
    }
  }

  render() {
    const tabsInfo = OccupancyReportHelper.getOccupancyReportItems(this.props.occupancyReportSummary);

    tabsInfo.onTabClickFn = this.onTabClick;
    tabsInfo.selectedTab = this.state.selectedTab;

    return (
      <div className="container">
        <Header
          showBackButton
          showSideBar
          pageTitle={pageTitle.OCCUPANCY_REPORT}
          onBackBtnClick={this.onBackBtnClick}
        />

        <OccupancyReportRoot
          isSummaryLoading={this.props.isSummaryLoading}
          reportStartDate={this.state.reportStartDate}
          reportEndDate={this.state.reportEndDate}
          tabsInfo={tabsInfo}
          onPreviousClick={this.onReportMonthSelection}
          onNextClick={this.onReportMonthSelection}
        />

        <ScrollToTop />

        <Footer />
      </div>
    );
  }
}

OccupancyReportPage.contextTypes = {
  router: PropTypes.object.isRequired,
};

OccupancyReportPage.defaultProps = {
  selectedHotel: {},
  isSummaryLoading: false,
  occupancyReportSummary: {},
  fetchOccupancyReportSummary() {},
  storeResetOccupancyReportSummary() {},
};

OccupancyReportPage.propTypes = {
  selectedHotel: PropTypes.object,
  isSummaryLoading: PropTypes.bool,
  occupancyReportSummary: PropTypes.object,
  fetchOccupancyReportSummary: PropTypes.func,
  storeResetOccupancyReportSummary: PropTypes.func,
};

function mapStateToProps(state) {
  const { occupancyReport, userInputs } = state;
  return {
    selectedHotel: userInputs.selectedHotel,
    isSummaryLoading: occupancyReport.isSummaryLoading,
    occupancyReportSummary: occupancyReport.occupancyReportSummary,
  };
}

export default connect(mapStateToProps, {
  fetchOccupancyReportSummary,
  storeResetOccupancyReportSummary,
})(OccupancyReportPage);
