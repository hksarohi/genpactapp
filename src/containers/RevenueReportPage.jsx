import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import Header from './../components/common/Header';
import Footer from './../components/common/Footer';

import RevenueReportRoot from './../components/revenueReport/RevenueReportRoot';

import { fetchRevenueReportSummary, storeResetRevenueReportSummary } from './../actions/RevenueReportAction';

import { pageTitle } from './../constants/Constants';
import RevenueReportHelper from './../helpers/RevenueReportHelper';
import DateUtils from './../utils/DateUtils';
import CommonHelper from './../helpers/CommonHelper';
import CommonUtils from './../utils/CommonUtils';
import ScrollToTop from './../components/common/ScrollToTop';

class RevenueReportPage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedTab: 1,
      reportStartDate: null,
      reportEndDate: null,
    };

    this.onTabClick = this.onTabClick.bind(this);
    this.onBackBtnClick = this.onBackBtnClick.bind(this);
    this.onReportMonthSelection = this.onReportMonthSelection.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    // This will be called whenever we change hotel in side bar
    const oldSelectedHotel = CommonHelper.getSelectedHotelId(this.props.selectedHotel);
    const newSelectedHotel = CommonHelper.getSelectedHotelId(nextProps.selectedHotel);

    if (oldSelectedHotel !== newSelectedHotel) {
      // For MG Properties, Revenue will not be displayed 
      const isMgProperty = CommonUtils.getObjectValue(nextProps.selectedHotel, 'mgProperty', false);
      if (isMgProperty) {
        this.context.router.history.push('/dashboard');
      }

      this.props.fetchRevenueReportSummary(
        newSelectedHotel, this.state.reportStartDate, this.state.reportEndDate);
    }
  }

  componentWillUnmount() {
    this.props.storeResetRevenueReportSummary();
  }

  onBackBtnClick() {
    this.context.router.history.push('/dashboard');
  }

  onReportMonthSelection(month) {
    const y = DateUtils.getDayMonthYear(this.state.reportEndDate).year;
    const m = month;

    const selectedDate = DateUtils.getInstance(`${y}-${m}-01`);
    const range = DateUtils.getMonthRangeDates(selectedDate);

    this.onTabClick(2, range.startDate, range.endDate);
  }

  onTabClick(id, startDate, endDate) {
    this.setState({
      selectedTab: id,
      reportStartDate: startDate,
      reportEndDate: endDate,
    });

    const propertyId = CommonHelper.getSelectedHotelId(this.props.selectedHotel);

    if (propertyId > 0) {
      this.props.fetchRevenueReportSummary(
        propertyId, startDate, endDate);
    }
  }

  render() {
    const tabsInfo = RevenueReportHelper.getRevenueReportItems(this.props.revenueReportSummary);

    tabsInfo.onTabClickFn = this.onTabClick;
    tabsInfo.selectedTab = this.state.selectedTab;

    return (
      <div className="container">
        <Header
          showBackButton
          showSideBar
          pageTitle={pageTitle.REVENUE_REPORT}
          onBackBtnClick={this.onBackBtnClick}
        />

        <RevenueReportRoot
          isSummaryLoading={this.props.isSummaryLoading}
          reportStartDate={this.state.reportStartDate}
          reportEndDate={this.state.reportEndDate}
          tabsInfo={tabsInfo}
          onPreviousClick={this.onReportMonthSelection}
          onNextClick={this.onReportMonthSelection}
        />

        <ScrollToTop />

        <Footer />
      </div>
    );
  }
}

RevenueReportPage.contextTypes = {
  router: PropTypes.object.isRequired,
};

RevenueReportPage.defaultProps = {
  selectedHotel: {},
  isSummaryLoading: false,
  revenueReportSummary: {},
  fetchRevenueReportSummary() {},
  storeResetRevenueReportSummary() {},
};

RevenueReportPage.propTypes = {
  selectedHotel: PropTypes.object,
  isSummaryLoading: PropTypes.bool,
  revenueReportSummary: PropTypes.object,
  fetchRevenueReportSummary: PropTypes.func,
  storeResetRevenueReportSummary: PropTypes.func,
};

function mapStateToProps(state) {
  const { revenueReport, userInputs } = state;
  return {
    selectedHotel: userInputs.selectedHotel,
    isSummaryLoading: revenueReport.isSummaryLoading,
    revenueReportSummary: revenueReport.revenueReportSummary,
  };
}

export default connect(mapStateToProps, {
  fetchRevenueReportSummary,
  storeResetRevenueReportSummary,
})(RevenueReportPage);
