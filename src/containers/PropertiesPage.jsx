import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { fetchOwnerHotelsIfNeeded, selectOwnerHotel } from './../actions/UserMetaAction';
import { fetchUserProfileIfNeeded } from './../actions/LoginProcessAction';

import PropertiesRoot from './../components/properties/PropertiesRoot';
import CommonUtils from './../utils/CommonUtils';
import Loader from './../components/common/Loader';

class PropertiesPage extends React.Component {
  constructor(props) {
    super(props);

    this.onPropertySelect = this.onPropertySelect.bind(this);
    this.props.fetchOwnerHotelsIfNeeded();
    this.props.fetchUserProfileIfNeeded();
  }

  onPropertySelect(hotelDetails) {
    this.props.selectOwnerHotel({ ...hotelDetails });
    this.context.router.history.push('/dashboard');
  }

  getUserFullName() {
    const fullName = `${CommonUtils.getObjectValueIfEmpty(this.props.userProfile, 'firstName', '')} ${
      CommonUtils.getObjectValueIfEmpty(this.props.userProfile, 'lastName', '')}`;

    return fullName;
  }

  render() {
    const userFullName = this.getUserFullName();
    const loader = this.props.isHotelsLoading ? (<Loader type={2} />) : null;

    return (
      <div className="login-wrapper">
        <div className="login properties-list">
          <span className="logo" />
          <h3>Welcome,</h3>
          <h2>{userFullName}</h2>
          {loader}
          <PropertiesRoot
            isHotelsLoading={this.props.isHotelsLoading}
            onPropertySelect={this.onPropertySelect}
            hotels={this.props.hotelList}
          />
        </div>
      </div>
    );
  }
}

PropertiesPage.defaultProps = {
  fetchOwnerHotelsIfNeeded() {},
  fetchUserProfileIfNeeded() {},
  selectOwnerHotel() {},
  hotelList: [],
  userProfile: {},
  isHotelsLoading: false,
};

PropertiesPage.propTypes = {
  fetchOwnerHotelsIfNeeded: PropTypes.func,
  fetchUserProfileIfNeeded: PropTypes.func,
  selectOwnerHotel: PropTypes.func,
  hotelList: PropTypes.array,
  userProfile: PropTypes.object,
  isHotelsLoading: PropTypes.bool,
};

PropertiesPage.contextTypes = {
  router: React.PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  const { userMeta, loginUser } = state;
  const hotelList = CommonUtils.getObjectValueIfEmpty(userMeta, 'hotels', []);

  return {
    hotelList,
    userProfile: loginUser.userData,
    isHotelsLoading: userMeta.isHotelsLoading,
  };
}

export default connect(mapStateToProps, {
  fetchOwnerHotelsIfNeeded,
  fetchUserProfileIfNeeded,
  selectOwnerHotel,
})(PropertiesPage);
