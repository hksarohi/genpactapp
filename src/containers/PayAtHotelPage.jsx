import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import Header from './../components/common/Header';
import Footer from './../components/common/Footer';

import PayAtHotelRoot from './../components/payAtHotel/PayAtHotelRoot';

import { fetchPayAtHotelSummary, storePayAtHotelSummary } from './../actions/PayAtHotelAction';
import { pageTitle } from './../constants/Constants';
import CommonUtils from './../utils/CommonUtils';
import CommonHelper from './../helpers/CommonHelper';
import PayAtHotelHelper from './../helpers/PayAtHotelHelper';
import ScrollToTop from './../components/common/ScrollToTop';

class PayAtHotelPage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      reportStartDate: null,
      reportEndDate: null,
    };

    this.onTabClick = this.onTabClick.bind(this);
    this.formatRowValue = this.formatRowValue.bind(this);
    this.onBackBtnClick = this.onBackBtnClick.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    // This will be called whenever we change hotel in side bar
    const oldSelectedHotel = CommonHelper.getSelectedHotelId(this.props.selectedHotel);
    const newSelectedHotel = CommonHelper.getSelectedHotelId(nextProps.selectedHotel);

    if (oldSelectedHotel !== newSelectedHotel) {
      this.props.fetchPayAtHotelSummary(
        newSelectedHotel, this.state.reportStartDate, this.state.reportEndDate);
    }
  }

  componentWillUnmount() {
    this.props.storePayAtHotelSummary();
  }

  onBackBtnClick() {
    this.context.router.history.push('/dashboard');
  }

  onTabClick(id, startDate, endDate) {
    this.setState({
      reportStartDate: startDate,
      reportEndDate: endDate,
    });

    const propertyId = CommonHelper.getSelectedHotelId(this.props.selectedHotel);
    if (propertyId > 0) {
      this.props.fetchPayAtHotelSummary(
        propertyId, startDate, endDate);
    }
  }

  getStatement() {
    const { payAtHotelSummary } = this.props;
    const statement = {};

    statement.headings = PayAtHotelHelper.getInfoTableHeadings();
    statement.itemKeys = PayAtHotelHelper.getInfoTableValueKeys();
    statement.items = CommonUtils.isEmpty(payAtHotelSummary) ? [] : [...payAtHotelSummary];

    return statement;
  }

  formatRowValue(key, item) {
    return PayAtHotelHelper.formatInfoTableRowValue(key, item);
  }

  render() {
    const statement = this.getStatement();

    return (
      <div className="container">
        <Header
          showLogo={false}
          pageTitle={pageTitle.PAY_AT_HOTEL}
          showBackButton
          onBackBtnClick={this.onBackBtnClick}
          showSideBar
        />

        <PayAtHotelRoot
          isSummaryLoading={this.props.isSummaryLoading}
          headings={statement.headings}
          itemKeys={statement.itemKeys}
          items={statement.items}
          onTabClickFn={this.onTabClick}
          formatRowValue={this.formatRowValue}
        />

        <ScrollToTop />

        <Footer />
      </div>
    );
  }
}

PayAtHotelPage.defaultProps = {
  selectedHotel: {},
  isSummaryLoading: false,
  payAtHotelSummary: [],
  fetchPayAtHotelSummary() {},
  storePayAtHotelSummary() {},
};

PayAtHotelPage.propTypes = {
  selectedHotel: PropTypes.object,
  isSummaryLoading: PropTypes.bool,
  payAtHotelSummary: PropTypes.arrayOf(PropTypes.object),
  fetchPayAtHotelSummary: PropTypes.func,
  storePayAtHotelSummary: PropTypes.func,
};

PayAtHotelPage.contextTypes = {
  router: React.PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  const { payAtHotel, userInputs } = state;
  return {
    selectedHotel: userInputs.selectedHotel,
    isSummaryLoading: payAtHotel.isSummaryLoading,
    payAtHotelSummary: payAtHotel.reportSummary,
  };
}

export default connect(mapStateToProps, {
  fetchPayAtHotelSummary,
  storePayAtHotelSummary,
})(PayAtHotelPage);
