import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import Header from './../components/common/Header';
import Footer from './../components/common/Footer';
import Loader from './../components/common/Loader';

import CustomerRatingsRoot from './../components/customerRatings/CustomerRatingsRoot';

import { fetchCustomerRatingsSummary, fetchCustomerReviews, storeResetCustomerRatings } from './../actions/CustomerRatingsAction';

import { pageTitle } from './../constants/Constants';
import CustomerRatingsHelper from './../helpers/CustomerRatingsHelper';
import CommonHelper from './../helpers/CommonHelper';
import DateUtils from './../utils/DateUtils';
import Pagination from './../utils/Pagination';
import ScrollToTop from './../components/common/ScrollToTop';

class CustomerRatingsPage extends React.Component {
  constructor(props) {
    super(props);

    const endDate = DateUtils.getDateInFormat(new Date(), DateUtils.yyyymmdd, '-');
    const startDate = DateUtils.getDateInFormat(DateUtils.add(endDate, { days: '-6' }), DateUtils.yyyymmdd, '-');

    this.state = {
      reportStartDate: startDate,
      reportEndDate: endDate,
      initialPage: 0,
      initialLimit: 10,
    };

    this.onBackBtnClick = this.onBackBtnClick.bind(this);
    this.paginateCustomerReviews = this.paginateCustomerReviews.bind(this);
  }

  componentDidMount() {
    /*
     * This will be called when we have already selected a hotel, then we
     * went to some other page, then we came back to this page again.
     */
    const selectedHotel = CommonHelper.getSelectedHotelId(this.props.selectedHotel);

    if (selectedHotel > 0) {
      this.props.fetchCustomerRatingsSummary(
        selectedHotel,
        this.state.reportStartDate,
        this.state.reportEndDate,
        this.state.initialPage,
        this.state.initialLimit,
      );
    }
  }


  componentWillReceiveProps(nextProps) {
    // This will be called whenever we change hotel in side bar
    const oldSelectedHotel = CommonHelper.getSelectedHotelId(this.props.selectedHotel);
    const newSelectedHotel = CommonHelper.getSelectedHotelId(nextProps.selectedHotel);

    if (oldSelectedHotel !== newSelectedHotel) {
      this.props.fetchCustomerRatingsSummary(newSelectedHotel,
        this.state.reportStartDate,
        this.state.reportEndDate,
        this.state.initialPage,
        this.state.initialLimit,
      );
    }
  }

  componentWillUnmount() {
    this.props.storeResetCustomerRatings();
  }

  onBackBtnClick() {
    this.context.router.history.push('/dashboard');
  }

  paginateCustomerReviews(pageNo, itemsPerPage) {
    const selectedHotel = CommonHelper.getSelectedHotelId(this.props.selectedHotel);
    this.props.fetchCustomerReviews(selectedHotel, pageNo, itemsPerPage);
  }

  render() {
    const loader = this.props.isSummaryLoading ? (<Loader type={1} />) : null;
    const reviewLoader = (this.props.isSummaryLoading || this.props.isReviewsLoading) ? (<Loader type={2} extraClassName="loader-blue" />) : null;
    const customerReviews = CustomerRatingsHelper.getCustomerRatingsList(this.props.customerRatingsSummary);
    const selectedHotelRatings = CustomerRatingsHelper.getSelectedHotelRatings(this.props.customerRatingsSummary);
    const fabHotelRatings = CustomerRatingsHelper.getFabHotelRatings(this.props.customerRatingsSummary);

    return (
      <div className="container">
        <Pagination
          page={this.state.initialPage}
          itemsPerPage={this.state.initialLimit}
          isAllPagesLoaded={this.props.isReviewsAllPagesLoaded}
          isItemsLoading={this.props.isReviewsLoading}
          fetchItems={this.paginateCustomerReviews}
        />

        <Header
          showLogo={false}
          pageTitle={pageTitle.CUSTOMER_RATINGS}
          showBackButton
          onBackBtnClick={this.onBackBtnClick}
          showSideBar
        />

        <CustomerRatingsRoot
          hotelRatings={selectedHotelRatings}
          fabhotelRatings={fabHotelRatings}
          customerReviews={customerReviews}
        />

        {reviewLoader}

        <ScrollToTop />

        <Footer />
        {loader}
      </div>
    );
  }
}

CustomerRatingsPage.defaultProps = {
  selectedHotel: {},
  customerRatingsSummary: null,
  isSummaryLoading: false,
  isReviewsLoading: false,
  isReviewsAllPagesLoaded: false,
  fetchCustomerRatingsSummary() {},
  fetchCustomerReviews() {},
  storeResetCustomerRatings() {},
};

CustomerRatingsPage.propTypes = {
  selectedHotel: PropTypes.object,
  customerRatingsSummary: PropTypes.object,
  isSummaryLoading: PropTypes.bool,
  isReviewsLoading: PropTypes.bool,
  isReviewsAllPagesLoaded: PropTypes.bool,
  fetchCustomerRatingsSummary: PropTypes.func,
  fetchCustomerReviews: PropTypes.func,
  storeResetCustomerRatings: PropTypes.func,
};

CustomerRatingsPage.contextTypes = {
  router: React.PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  const { customerRatings, userInputs } = state;
  return {
    selectedHotel: userInputs.selectedHotel,
    customerRatingsSummary: customerRatings.customerRatingsSummary,
    isSummaryLoading: customerRatings.isSummaryLoading,
    isReviewsLoading: customerRatings.isReviewsLoading,
    isReviewsAllPagesLoaded: customerRatings.isReviewsAllPagesLoaded,
  };
}

export default connect(mapStateToProps, {
  fetchCustomerRatingsSummary,
  fetchCustomerReviews,
  storeResetCustomerRatings,
})(CustomerRatingsPage);
