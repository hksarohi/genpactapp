import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import Header from './../components/common/Header';
import DashboardRoot from './../components/dashboard/DashboardRoot';
import Footer from './../components/common/Footer';
import Loader from './../components/common/Loader';

import { fetchHotelSummary, storeResetHotelSummary } from './../actions/DashboardAction';
import { pageTitle } from './../constants/Constants';
import DashboardHelper from './../helpers/DashboardHelper';
import CommonHelper from './../helpers/CommonHelper';
import DateUtils from './../utils/DateUtils';

class DashboardPage extends React.Component {
  constructor(props) {
    super(props);

    const endDate = DateUtils.getDateInFormat(new Date(), DateUtils.yyyymmdd, '-');
    const startDate = DateUtils.getDateInFormat(DateUtils.add(endDate, { days: '-6' }), DateUtils.yyyymmdd, '-');

    this.state = {
      reportStartDate: startDate,
      reportEndDate: endDate,
    };
  }

  // componentWillMount() {
  //   document.body.classList.add('go-blue');
  // }

  componentDidMount() {
    /*
    * This will be called when we have already selected a hotel, then we
    * went to some other page, then we came back to this page again.
    */
    const selectedHotel = CommonHelper.getSelectedHotelId(this.props.selectedHotel);
    if (selectedHotel > 0) {
      this.props.fetchHotelSummary(
        selectedHotel, this.state.reportStartDate, this.state.reportEndDate);
    }
  }

  componentWillReceiveProps(nextProps) {
    // this will be called whenever we change hotelin side bar
    const oldSelectedHotel = CommonHelper.getSelectedHotelId(this.props.selectedHotel);
    const newSelectedHotel = CommonHelper.getSelectedHotelId(nextProps.selectedHotel);

    if (oldSelectedHotel !== newSelectedHotel) {
      this.props.fetchHotelSummary(
        newSelectedHotel, this.state.reportStartDate, this.state.reportEndDate);
    }
  }


  componentWillUnmount() {
    this.props.storeResetHotelSummary();
    // document.body.classList.remove('go-blue');
  }

  render() {
    const loader = this.props.isMenuListLoading ? (<Loader type={1} />) : null;
    const menuList = DashboardHelper.getMenuList(this.props.hotelSummary, this.props.selectedHotel);

    return (
      <div className="container">
        <Header
          showLogo
          pageTitle={pageTitle.DASHBOARD}
          showBackButton={false}
          showSideBar
        />

        <DashboardRoot menuCardList={menuList} />

        <Footer />
        {loader}
      </div>
    );
  }
}

DashboardPage.defaultProps = {
  hotelSummary: null,
  isMenuListLoading: false,
  selectedHotel: {},
  fetchHotelSummary() {},
  storeResetHotelSummary() {},
};

DashboardPage.propTypes = {
  hotelSummary: PropTypes.object,
  selectedHotel: PropTypes.object,
  isMenuListLoading: PropTypes.bool,
  fetchHotelSummary: PropTypes.func,
  storeResetHotelSummary: PropTypes.func,
};

function mapStateToProps(state) {
  const { dashboard, userInputs } = state;
  return {
    hotelSummary: dashboard.hotelSummary,
    isMenuListLoading: dashboard.isMenuListLoading,
    selectedHotel: userInputs.selectedHotel,
  };
}

export default connect(mapStateToProps, {
  fetchHotelSummary,
  storeResetHotelSummary,
})(DashboardPage);
