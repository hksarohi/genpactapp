import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import Header from './../components/common/Header';
import Footer from './../components/common/Footer';

import UnderMaintenanceRoomsRoot from './../components/underMaintenanceRooms/UnderMaintenanceRoomsRoot';

import { fetchUnderMaintenanceRoomsSummary, storeResetUnderMaintenanceRooms } from './../actions/UnderMaintenanceRoomsAction';
import { pageTitle } from './../constants/Constants';
import UnderMaintenanceRoomsHelper from './../helpers/UnderMaintenanceRoomsHelper';
import CommonUtils from './../utils/CommonUtils';
import CommonHelper from './../helpers/CommonHelper';
import ScrollToTop from './../components/common/ScrollToTop';

class UnderMaintenanceRoomsPage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      reportStartDate: null,
      reportEndDate: null,
    };

    this.onTabClick = this.onTabClick.bind(this);
    this.onBackBtnClick = this.onBackBtnClick.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    // This will be called whenever we change hotel in side bar
    const oldSelectedHotel = CommonHelper.getSelectedHotelId(this.props.selectedHotel);
    const newSelectedHotel = CommonHelper.getSelectedHotelId(nextProps.selectedHotel);

    if (oldSelectedHotel !== newSelectedHotel) {
      this.props.fetchUnderMaintenanceRoomsSummary(
        newSelectedHotel, this.state.reportStartDate, this.state.reportEndDate);
    }
  }

  componentWillUnmount() {
    this.props.storeResetUnderMaintenanceRooms();
  }

  onBackBtnClick() {
    this.context.router.history.push('/dashboard');
  }

  onTabClick(id, startDate, endDate) {
    this.setState({
      reportStartDate: startDate,
      reportEndDate: endDate,
    });

    const propertyId = CommonHelper.getSelectedHotelId(this.props.selectedHotel);

    if (propertyId > 0) {
      this.props.fetchUnderMaintenanceRoomsSummary(
        propertyId, startDate, endDate);
    }
  }

  getStatement() {
    const { roomsSummary } = this.props;
    const statement = {};

    statement.items = CommonUtils.isEmpty(roomsSummary) ? {} : { ...roomsSummary };

    return statement;
  }

  render() {
    const roomsList = UnderMaintenanceRoomsHelper.getRoomsList(this.props.todaySummary, this.props.yesterdaySummary);
    const tabsInfo = this.getStatement();
    tabsInfo.onTabClickFn = this.onTabClick;

    return (
      <div className="container">
        <Header
          showLogo={false}
          pageTitle={pageTitle.UNDER_MAINTENANCE_ROOMS}
          showBackButton
          onBackBtnClick={this.onBackBtnClick}
          showSideBar
        />

        <UnderMaintenanceRoomsRoot
          isRoomsLoading={this.props.isRoomsLoading}
          reportStartDate={this.state.reportStartDate}
          reportEndDate={this.state.reportEndDate}
          roomsInfo={roomsList.roomsInfo}
          tabsInfo={tabsInfo}
        />

        <ScrollToTop />

        <Footer />
      </div>
    );
  }
}

UnderMaintenanceRoomsPage.defaultProps = {
  selectedHotel: {},
  roomsSummary: {},
  todaySummary: {},
  yesterdaySummary: {},
  isRoomsLoading: false,
  fetchUnderMaintenanceRoomsSummary() {},
  storeResetUnderMaintenanceRooms() {},
};

UnderMaintenanceRoomsPage.propTypes = {
  selectedHotel: PropTypes.object,
  roomsSummary: PropTypes.object,
  todaySummary: PropTypes.object,
  yesterdaySummary: PropTypes.object,
  isRoomsLoading: PropTypes.bool,
  fetchUnderMaintenanceRoomsSummary: PropTypes.func,
  storeResetUnderMaintenanceRooms: PropTypes.func,
};

UnderMaintenanceRoomsPage.contextTypes = {
  router: React.PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  const { underMaintenanceRooms, userInputs } = state;

  return {
    selectedHotel: userInputs.selectedHotel,
    roomsSummary: underMaintenanceRooms.underMaintenanceRoomsSummary,
    todaySummary: underMaintenanceRooms.todayOutOfOrderSummary,
    yesterdaySummary: underMaintenanceRooms.yesterdayOutOfOrderSummary,
    isRoomsLoading: underMaintenanceRooms.isUnderMaintenanceRoomsLoading,
  };
}

export default connect(mapStateToProps, {
  fetchUnderMaintenanceRoomsSummary,
  storeResetUnderMaintenanceRooms,
})(UnderMaintenanceRoomsPage);
