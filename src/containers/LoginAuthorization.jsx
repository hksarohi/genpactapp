import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Route, Redirect } from 'react-router-dom';

import AuthenticationHelper from './../helpers/AuthenticationHelper';

const LoginAuthorization = ({ component, ...props }) => {
  if (!AuthenticationHelper.isAuthenticated() || !props.isAuthenticated) {
    return (<Redirect to="/login" />);
  }

  return (<Route {...props} component={component} />);
};

LoginAuthorization.defaultProps = {
  isAuthenticated: true,
  component() {},
};

LoginAuthorization.propTypes = {
  isAuthenticated: PropTypes.bool,
  component: PropTypes.func,
};

function mapStateToProps(state) {
  const { loginUser } = state;

  return {
    isAuthenticated: loginUser.isAuthenticated,
  };
}

export default connect(mapStateToProps)(LoginAuthorization);
