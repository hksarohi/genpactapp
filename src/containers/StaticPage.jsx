import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import Header from './../components/common/Header';
import Footer from './../components/common/Footer';
import { pageTitle } from './../constants/Constants';
import HelpRoot from './../components/static/HelpRoot';

class StaticPage extends React.Component {
  constructor(props) {
    super(props);

    this.onBackBtnClick = this.onBackBtnClick.bind(this);
  }

  onBackBtnClick() {
    this.context.router.history.push('/dashboard');
  }

  getStaticComponent() {
    const loc = this.props.location.pathname;

    if (loc === '/help') {
      return (<HelpRoot />);
    }

    return '';
  }

  render() {
    const element = this.getStaticComponent();
    return (
      <div className="container">
        <Header
          showLogo={false}
          pageTitle={pageTitle.HELP}
          showBackButton
          onBackBtnClick={this.onBackBtnClick}
          showSideBar={false}
        />

        {element}

        <Footer />
      </div>
    );
  }
}

StaticPage.defaultProps = {
  location: {},
};

StaticPage.propTypes = {
  location: PropTypes.object,
};

StaticPage.contextTypes = {
  router: React.PropTypes.object.isRequired,
};

export default connect(null)(StaticPage);
