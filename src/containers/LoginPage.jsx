import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { loginRequest, loginOtpValidationRequest, loginOtpResendRequest, loginResetStore } from './../actions/LoginProcessAction';

import LoginRoot from './../components/login/LoginRoot';

class LoginPage extends React.Component {
  constructor(props) {
    super(props);

    this.onLoginSubmit = this.onLoginSubmit.bind(this);
    this.onLoginOtpSubmit = this.onLoginOtpSubmit.bind(this);
    this.onLoginOtpResend = this.onLoginOtpResend.bind(this);
    this.onLoginOtpCancel = this.onLoginOtpCancel.bind(this);
    this.onOtpLimitExceeded = this.onOtpLimitExceeded.bind(this);

    /*
    * RESET STORE PARAMS when user in not authenticated and visits login screen.
    *  This is needed in case he goes back to login(mobile) screen from OTP/any screen
    */
    this.props.loginResetStore();
  }

  componentWillReceiveProps(nextProps) {
    // if isAuthenticated is true, then token must be present
    if (nextProps.isAuthenticated) {
      /*
      * RESET STORE PARAMS ON SUCCESSFUL 1 or 2 FACTOR LOGIN
      * like isLoggingIn, isOtpValidating, isOtpValidated etc
      * except isAuthenticated and user profile
      */
      this.props.loginResetStore();
      this.context.router.history.push('/properties');
    }
  }

  onLoginSubmit(data) {
    const params = {
      isoCode: data.isoCode,
      countryCode: data.countryCode,
      mobile: data.mobile,
      userType: 'HOTELIER',
    };
    this.props.loginRequest(params);
  }

  onLoginOtpSubmit(data) {
    const params = {
      isoCode: this.props.userData.isoCode,
      countryCode: this.props.userData.countryCode,
      mobile: this.props.userData.mobile,
      otp: data.otp,
      userType: 'HOTELIER',
    };
    this.props.loginOtpValidationRequest(params);
  }

  onLoginOtpCancel() {
    this.props.loginResetStore();
  }

  onOtpLimitExceeded() {
    this.props.loginResetStore();
  }

  onLoginOtpResend() {
    const params = {
      isoCode: this.props.userData.isoCode,
      countryCode: this.props.userData.countryCode,
      mobile: this.props.userData.mobile,
      userType: 'HOTELIER',
    };
    this.props.loginOtpResendRequest(params);
  }

  shouldShowOTPForm() {
    return (this.props.isLoggedIn && !this.props.isOtpValidated);
  }

  render() {
    return (
      <div className="login-wrapper">
        <LoginRoot
          showOtpForm={this.shouldShowOTPForm()}
          isLoggedIn={this.props.isLoggedIn}
          invalidOtpCount={this.props.invalidOtpCount}
          isLoginFormSubmitting={this.props.isLoginFormSubmitting}
          isOTPFormSubmitting={this.props.isOTPFormSubmitting}
          isResendingOtp={this.props.isResendingOtp}
          onLoginSubmit={this.onLoginSubmit}
          onLoginOtpSubmit={this.onLoginOtpSubmit}
          onLoginOtpResend={this.onLoginOtpResend}
          onLoginOtpCancel={this.onLoginOtpCancel}
          onOtpLimitExceeded={this.onOtpLimitExceeded}
        />
      </div>
    );
  }
}

LoginPage.defaultProps = {
  isLoginFormSubmitting: false,
  isOTPFormSubmitting: false,
  isResendingOtp: false,
  isLoggedIn: false,
  isOtpValidated: false,
  invalidOtpCount: 0,
  isAuthenticated: false,
  userData: {},
  loginRequest() {},
  loginOtpValidationRequest() {},
  loginOtpResendRequest() {},
  loginResetStore() {},
};

LoginPage.propTypes = {
  isLoginFormSubmitting: PropTypes.bool,
  isOTPFormSubmitting: PropTypes.bool,
  isResendingOtp: PropTypes.bool,
  isLoggedIn: PropTypes.bool,
  isOtpValidated: PropTypes.bool,
  invalidOtpCount: PropTypes.number,
  isAuthenticated: PropTypes.bool,
  userData: PropTypes.object,
  loginRequest: PropTypes.func,
  loginOtpValidationRequest: PropTypes.func,
  loginOtpResendRequest: PropTypes.func,
  loginResetStore: PropTypes.func,
};

LoginPage.contextTypes = {
  router: React.PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  const { loginUser } = state;
  return {
    isLoginFormSubmitting: loginUser.isLoggingIn,
    isOTPFormSubmitting: loginUser.isOtpValidating,
    isResendingOtp: loginUser.isResendingOtp,
    isLoggedIn: loginUser.isLoggedIn,
    isOtpValidated: loginUser.isOtpValidated,
    invalidOtpCount: loginUser.invalidOtpCount,
    isAuthenticated: loginUser.isAuthenticated,
    userData: loginUser.userData,
  };
}

export default connect(mapStateToProps, {
  loginRequest,
  loginOtpValidationRequest,
  loginOtpResendRequest,
  loginResetStore,
})(LoginPage);
