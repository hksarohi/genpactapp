import keyMirror from 'keymirror';

export const actions = keyMirror({
  LOGIN_RESET_STORE: null,

  LOGOUT_REQUEST: null,
  LOGIN_REQUEST_INIT: null,
  LOGIN_REQUEST_SUCCESS: null,
  LOGIN_REQUEST_FAILED: null,

  LOGIN_OTP_VALIDATION_INIT: null,
  LOGIN_OTP_VALIDATION_SUCCESS: null,
  LOGIN_OTP_VALIDATION_FAILED: null,

  LOGIN_OTP_RESEND_INIT: null,
  LOGIN_OTP_RESEND_SUCCESS: null,
  LOGIN_OTP_RESEND_FAILED: null,

  USER_PROFILE_FETCH_REQUEST_INIT: null,
  USER_PROFILE_FETCH_REQUEST_SUCCESS: null,
  USER_PROFILE_FETCH_REQUEST_FAILED: null,

  DASHBOARD_ITEMS_LOADING: null,
  DASHBOARD_ITEMS_LOADED: null,
  DASHBOARD_ITEMS_LOADING_FAILED: null,
  DASHBOARD_ITEMS_STORE_RESET: null,

  OWNER_HOTELS_LOADING: null,
  OWNER_HOTELS_LOADED: null,
  OWNER_HOTELS_LOADING_FAILED: null,
  OWNER_CHANGED_SELECTED_HOTEL: null,

  CUSTOMER_RATINGS_LOADING: null,
  CUSTOMER_RATINGS_LOADED: null,
  CUSTOMER_RATINGS_LOADING_FAILED: null,
  CUSTOMER_RATINGS_STORE_RESET: null,

  CUSTOMER_REVIEWS_LOADING: null,
  CUSTOMER_REVIEWS_LOADED: null,
  CUSTOMER_REVIEWS_LOADING_FAILED: null,

  UNDER_MAINTENANCE_ROOMS_LOADING: null,
  UNDER_MAINTENANCE_ROOMS_LOADED: null,
  UNDER_MAINTENANCE_ROOMS_LOADING_FAILED: null,
  UNDER_MAINTENANCE_ROOMS_STORE_RESET: null,

  REVENUE_REPORT_LOADING: null,
  REVENUE_REPORT_LOADED: null,
  REVENUE_REPORT_LOADING_FAILED: null,
  REVENUE_REPORT_STORE_RESET: null,

  OCCUPANCY_REPORT_LOADING: null,
  OCCUPANCY_REPORT_LOADED: null,
  OCCUPANCY_REPORT_LOADING_FAILED: null,
  OCCUPANCY_REPORT_STORE_RESET: null,

  PAY_AT_HOTEL_LOADING: null,
  PAY_AT_HOTEL_LOADED: null,
  PAY_AT_HOTEL_LOADING_FAILED: null,
  PAY_AT_HOTEL_SUMMARY_STORE_RESET: null,
});

export const others = keyMirror({
});

export const assetUrl = {
  IMAGE_BASE_URL: 'https://pimg.fabhotels.com/medium/',
};

export const apiUrl = {
  BASE_URL: '',
  LOGIN_HOTELIER: '/fabuser/user/login',
  LOGIN_OTP_VALIDATION: '/fabuser/user/validateOtp',
  LOGIN_OTP_RESEND: '/fabuser/user/resendOtp',
  FETCH_USER_PROFILE: '/fabuser/user/getUserProfile',
  LOGOUT_HOTELIER: '/fabuser/user/signOut',
  PROPERTY_LIST: '/fabhotelier/property',
  HOTEL_PERFORMANCE_SUMMARY: '/fabhotelier/dashboard/{propertyId}/{startDate}/{endDate}',
  CUSTOMER_RATINGS_SUMMARY: '/fabhotelier/hotel/review/{propertyId}/{startDate}/{endDate}/{page}/{limit}',
  CUSTOMER_REVIEWS: '/fabhotelier/page/review/{propertyId}/{page}/{limit}',
  UNDER_MAINTENANCE_ROOMS_SUMMARY: '/fabhotelier/outoforder/room/{propertyId}/{startDate}/{endDate}',
  REVENUE_REPORT_SUMMARY: '/fabhotelier/booking/avg/price/{propertyId}/{startDate}/{endDate}',
  OCCUPANCY_REPORT_SUMMARY: '/fabhotelier/booking/number/{propertyId}/{startDate}/{endDate}',
  PAY_AT_HOTEL_SUMMARY: '/fabhotelier/booking/payathotel/{propertyId}/{startDate}/{endDate}',
};

export const pageTitle = {
  DASHBOARD: '',
  CUSTOMER_RATINGS: 'Customer Ratings',
  UNDER_MAINTENANCE_ROOMS: 'Under Maintenance Rooms',
  OCCUPANCY_REPORT: 'Occupancy',
  REVENUE_REPORT: 'ADR',
  PAY_AT_HOTEL: 'Pay @ Hotel',
  HELP: 'Help',
};

export const httpStatusCodes = {
  UNAUTHORIZED: 401,
  BAD_REQUEST: 400,
};
